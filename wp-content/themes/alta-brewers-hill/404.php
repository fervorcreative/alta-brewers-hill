<?php
/**
 * The template for displaying 404 pages (not found)
 */

get_header(); ?>
<main id="main" role="main">
<?php get_template_part('partials/content', 'banner' ); ?>
<section class="section error error-404 not-found">
  <div class="grid-container">
    <header class="grid-x">
      <div class="cell">
        <h1 class="m-b-20"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.'); ?></h1>
      </div>
    </header>

    <div class="grid-x">
      <div class="cell">
        <p class="m-b-20"><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?'); ?></p>

        <?php get_search_form(); ?>
      </div>
    </div>
  </div>
</section>
</main>
<?php get_footer(); ?>
