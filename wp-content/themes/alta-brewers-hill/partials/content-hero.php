<?php
$page_id = ( is_front_page() ) ? get_option( 'page_on_front' ) : get_the_ID();
$hero_class = ( is_front_page() ) ? 'home' : 'page';
$hero = get_field( 'hero', $page_id );
$hero_img = $hero['background_image'];
$hero_img_src = wp_get_attachment_image_src( $hero_img, 'full' );
/*
 * vertical positioning
 * options: top, center, bottom
 * defaults to center
 */
$hero_img_pos = $hero['background_position']; 
$hero_gfx = $hero['logo_graphic']; // array
$hero_title = $hero['title'];

?>
<section class="hero <?php echo $hero_class; ?>" style="background-image: url(<?php echo $hero_img_src[0]; ?>); background-position: center <?php echo $hero_img_pos; ?>">
    <?php if( is_front_page() ) { ?>
        <img
            src="<?php bloginfo('template_directory'); ?>/assets/images/logo-crown-large.svg"
            class="hero__logo-mark"
            alt="Alta Brewers Hill mark"
            aria-hidden>
        <div class="video-wrap">
            <video class="video video--hero" autoplay loop muted poster="<?php echo $hero_img_src[0]; ?>">
                <source src="<?php echo get_template_directory_uri(); ?>/media/abh-video_2.mp4" type="video/mp4">
                <source src="<?php echo get_template_directory_uri(); ?>/media/abh-video_2.webm" type="video/webm">
                <source src="<?php echo get_template_directory_uri(); ?>/media/abh-video_2.ogv" type="video/ogg">
            </video>
        </div>
    <?php } ?>
    <?php if( ! empty($hero_gfx) ) { ?>
    <div class="hero__logo">
        <?php echo wp_get_attachment_image( $hero_gfx['ID'], 'full' ); ?>
    </div>
    <?php } ?>
    
    <?php 
    if( ! is_front_page() ) {
        if( !empty($hero_title) ) { ?>
            <h1 class="page-title text-center" data-aos="fade-up"><?php echo $hero_title; ?></h1>
        <?php } else { 
            the_title( '<h1 class="page-title text-center"  data-aos="fade-up">', '</h1>' );
        } 
    }?>
</section>