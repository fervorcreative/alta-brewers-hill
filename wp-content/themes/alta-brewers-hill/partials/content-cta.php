<section class="section" style="z-index: 400">

    <div class="grid-container">
        <div class="grid-x grid-padding-y align-center">
            <div class="cell text-center" data-aos="fade-up">
                <h2><?php the_field( 'cta_title', 'option' ); ?></h2>
            </div>
        </div>
        <div class="grid-x grid-padding-y">
            <div class="cell large-5 large-offset-2" data-aos="fade-right">
                <?php the_field( 'cta_content', 'option' ); ?>
            </div>
            <div class="cell large-4  large-offset-1" data-aos="fade-left">
            <?php 
                $button = get_field( 'cta_button', 'option' );
                $btn_txt = $button['button_text'];
                $btn_url = $button['button_link'];
                if( !empty( $btn_txt ) && !empty( $btn_url ) ) {
                    echo '<p><a href="'. esc_url( $btn_url ).'" class="button">'. esc_html( $btn_txt ) .'</a></p>';
                }
                ?>
            </div>
        </div>
    </div>
</section>