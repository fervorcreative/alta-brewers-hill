<?php if( have_rows('content_sections') ) : ?>
    <?php while( have_rows('content_sections') ) : the_row(); ?>
    
        <?php if( get_row_layout() == 'gradient_headline' ) : ?>
            
            <section class="section section--gradient-headline section--no-pad-bottom section--keep-top">
                <div class="scroll-text-container">
                    <h2 class="scroll-text scroll-text--main display-h1 color-primary js-scroll-across"><?php the_sub_field( 'headline' ); ?></h2>
                </div>
                <div class="grid-container">
                    <div class="intro grid-x grid-y-padding align-center">
                    
                        <div class="cell text-center" data-aos="fade-up">
                            <h1 class="display-h2"><?php the_sub_field( 'subheading' ); ?></h1>
                        </div>
                    </div>
                </div>
            </section>
        <?php elseif( get_row_layout() == 'image_block' ) : ?>
            <?php $crown_pattern = get_sub_field( 'crown_pattern' ); ?>
            <section class="section section--image-block image-block section--no-pad">
                <div class="grid-container  <?php if( $crown_pattern !== 'none' ) echo 'pattern pattern--' . $crown_pattern; ?>">
                    <div class="grid-x grid-y-padding">
                        <div class="cell" data-aos="fade-up">
                            <?php 
                            $image = get_sub_field( 'image' ); 
                            echo wp_get_attachment_image( $image['ID'], 'full' ); ?>
                        </div>
                    </div>
                </div>
            </section>
        <?php elseif( get_row_layout() == 'text_block' ) : ?>
            <?php $center_copy = get_sub_field( 'center_copy' );
            $desc = get_sub_field( 'description' ); 
            ?>
            <section class="section section--text-block">
                <div class="grid-container">
                    <div class="grid-x grid-y-padding <?php if( $center_copy ) echo 'align-center'; ?>">
                        <div class="cell <?php if( $desc ) echo 'medium-7'; ?>">
                            <h2 class="display-h2 <?php if( $desc ) echo 'm-b-1'; ?> <?php if( $center_copy ) echo 'text-center'; ?>"><?php the_sub_field( 'headline' ); ?></h2>
                            <?php if( ! empty( $desc ) ) { ?>
                                <div class="<?php if( $center_copy ) echo 'text-center'; ?>"><?php echo $desc; ?></div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </section>
            
        <?php elseif( get_row_layout() == 'image_and_text' ) : ?>
            <?php
            $c_title_size = get_sub_field( 'content_headline_size' ); // returns small, medium, or large
            $content_pos = get_sub_field( 'content_position' ); // returns left or right
            $flush_image = get_sub_field( 'image_alignment' ); // boolean
            $overlap = get_sub_field( 'offset_content' ); // boolean
            
            $text_col_class = ($overlap) ? 'large-6' : 'large-5 medium-offset-1';
            $img_col_class = ($overlap) ? '' : '';
            $order_class = ($content_pos == 'right') ? 'medium-order-2' : '';
            $text_aos_class = ($content_pos == 'right') ? 'fade-left' : 'fade-right';
            $img_aos_class = ($content_pos == 'right') ? 'fade-right' : 'fade-left';
            
            $add_ft_list = get_sub_field( 'add_list_of_features' );
            $ft_group = get_sub_field('features');
            $ft_title = $ft_group['features_title'];
            $ft_text = $ft_group['features_list'];
            
            $crown_pattern = get_sub_field( 'crown_pattern' );
            $crown_pattern_image = get_sub_field( 'crown_pattern_below_image' );
            
            ?>
            <section class="section section--image-text <?php //if( !$flush_image) echo 'section--no-pad'; ?> <?php if( $crown_pattern !== 'none' ) echo 'pattern pattern--' . $crown_pattern; ?>">
                <div class="grid-container">
                    <?php if( get_sub_field( 'section_headline' ) ) { ?>
                        <h2 class="display-h1 scroll-text js-scroll-across"><?php the_sub_field( 'section_headline' ); ?></h2>
                    <?php } ?>

                    <div class="grid-x grid-y-padding <?php if( ! $add_ft_list ) echo 'align-middle'; ?>">
                        <div class="cell medium-5 <?php 
                        if( $content_pos == 'right') {
                            echo 'medium-offset-1 ';
                        } else {
                            if( $flush_image && $overlap ) {
                                echo 'overlap overlap--right';
                            } elseif( $flush_image ) {
                                echo 'large-4';
                            }
                        } ?> <?php echo $order_class; ?> <?php if( $add_ft_list ) echo 'm-t-3 m-b-3'; ?>" data-aos="<?php echo $text_aos_class; ?>">
                            <div class="inner">
                                <h3 class="entry-title <?php echo $c_title_size; ?>"><?php the_sub_field( 'headline' ); ?></h3>
                                <?php the_sub_field( 'description' ); ?>
                                <?php 
                                // $button = get_sub_field( 'button' );
                                // $btn_txt = $button['button_text'];
                                // $btn_url = $button['button_url'];
                                // if( !empty( $btn_txt ) && !empty( $btn_url ) ) {
                                //     echo '<p><a href="'. esc_url( $btn_url ).'" class="button">'. esc_html( $btn_txt ) .'</a></p>';
                                // }
                                $buttons = get_sub_field( 'buttons' );
                                if( $buttons ) {
                                    foreach( $buttons as $button ) {
                                        echo '<p><a href="'. esc_url( $button['button_link'] ) .'" class="button">'. $button['button_text'] .'</a></p>';
                                    }
                                }
                                ?>
                                
                                
                                <h4 class="m-t-3"><?php echo $ft_title; ?></h4>
                                <?php echo $ft_text; ?>
                                
                            </div>
                        </div>
                        <div class="cell medium-6 <?php if( $content_pos == 'right') {
                            echo '';
                        } elseif( $flush_image && $overlap ) {
                            echo 'flush-column';
                        } elseif( $flush_image ) {
                            echo 'large-offset-1 flush-column';
                        } else {
                            echo 'medium-offset-1';
                        } ?>" data-aos="<?php echo $img_aos_class; ?>">
                        <?php if($flush_image) echo '<div class="image-flush">'; ?>
                        <?php if( $flush_image && $crown_pattern_image ) echo '<div class="image-flush pattern pattern--full">'; ?>
                        <?php 
                            $image = get_sub_field( 'image' ); 
                            echo wp_get_attachment_image( $image['ID'], 'large' ); ?>
                        </div>
                        <?php if( $flush_image ) echo '</div>'; ?>
                    </div>
                </div>
            </section>
        <?php elseif( get_row_layout() == 'image_slider' ) : ?>
            <?php
            $crown_pattern = get_sub_field( 'crown_pattern' );
            ?>
            <section class="section section--image-slider section--no-pad section--small-top-mobile <?php if( $crown_pattern != 'none' ) echo 'pattern pattern--' . $crown_pattern; ?>">
                <div class="grid-x grid-y-padding">
                    <div class="cell custom-slider">
                        <!-- <div class="slider" data-items="1" data-edgepadding="300" data-gutter="16"> -->
                        <div class="slider slider--carousel">
                            <?php
                                $images = get_sub_field( 'images' );
                                foreach( $images as $image ) : 
                                    echo '<div>'. wp_get_attachment_image( $image['ID'], 'carousel' ) .'</div>';
                                endforeach;
                            ?>
                        </div>
                    </div>
                </div>
            </section>
        <?php elseif( get_row_layout() == 'image_gallery' ) : ?>
        <section class="section section--image-gallery section--no-pad section--keep-bottom image-gallery" style="display: none;">
            <div class="grid-container pattern pattern--double">
                <div class="grid-x grid-y-padding">
                    <div class="cell small-12 text-center">
                        <h3 class="display-h2 m-t-2 m-b-6"><?php the_sub_field( 'gallery_title'); ?></h3>
                    </div>
                    
                    <?php
                        $i=0; 
                        while( have_rows('gallery') ) : the_row(); $i++;
                            $galname = get_sub_field( 'gallery_name');
                            $galslug = str_replace( ' ', '-', $galname );
                            $galslug = strtolower( $galslug ); ?>
                            
                            <?php 
                            $images = get_sub_field( 'gallery_images' ); 
                            foreach( $images as $image ) : ?>
                                <?php 
                                $slide_img = wp_get_attachment_image_src( $image['ID'], 'slider-crop' ); ?>
                                <div class="cell small-6 medium-4 large-3 image-gal">
                                    <a href="<?php echo wp_get_attachment_image_url( $image['ID'], 'large' ); ?>" class="js-zoom">
                                        <?php echo wp_get_attachment_image( $image['ID'], 'thumbnail' ); ?>
                                    </a>
                                </div>
                            <?php endforeach; ?>
                        <?php endwhile; ?>
                    </div>
                </div>
            </div>
        </section>
        <?php elseif( get_row_layout() == 'iframe_embeds' ) : ?>
        <section class="section section--iframe section--no-pad-top">
            <div class="grid-container">
                <div class="grid-x grid-margin-x align-center section section--no-pad section--keep-bottom">
                    <div class="cell text-center" data-aos="fade-up">
                        <h2><?php the_sub_field( 'section_title' ); ?></h2>
                    </div>
                </div>
            </div>
            <div class="grid-container embed-wrapper">
                <div class="grid-x grid-margin-x align-center">
                    <?php $j=0; while( have_rows('media') ) : the_row(); $j++; ?>
                        <div class="cell small-6 large-3">
                            <div class="embed-container">
                                <a href="#embed-<?php echo $j; ?>" class="js-inline">
                                <?php
                                $iframe_code = get_sub_field( 'iframe_code' );
                                $img = get_sub_field( 'media_thumbnail' );
                                echo wp_get_attachment_image( $img['ID'], 'medium' );
                                ?>
                                </a>
                                <h4><?php the_sub_field( 'media_title' ); ?></h4>
                                <div id="embed-<?php echo $j; ?>" class="mfp-hide inline-embed">
                                    <?php echo $iframe_code; ?>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                </div>
            </div>
        </section>
        <?php elseif( get_row_layout() == 'featured_highlights' ) : ?>
            <section class="section section--ft-highlights">
                <div class="grid-container">
                    <div class="grid-x grid-margin-x align-center section section--no-pad section--keep-bottom">
                        <div class="cell text-center" data-aos="fade-up">
                            <h2><?php the_sub_field( 'headline' ); ?></h2>
                        </div>
                    </div>
                    <?php if( have_rows( 'special_callouts') ) : ?>
                        <div class="grid-x grid-margin-x">
                            <?php while( have_rows( 'special_callouts') ) : the_row(); ?>
                                <div class="cell small-12 large-auto text-center m-b-2"  data-aos="fade-up">
                                    <h4 class="display-h5 text-uppercase"><strong><?php the_sub_field( 'callout_title' ); ?></strong></h4>
                                    <p class="font-sans smaller-text"><?php the_sub_field( 'callout_subtitle' ); ?></p>
                                </div>
                            <?php endwhile; ?>
                        </div>
                    <?php endif; ?>
                    <?php if( have_rows('additional_highlights') ) : ?>
                        
                        <?php while( have_rows('additional_highlights') ) : the_row(); ?>
                        
                            <?php if( get_sub_field( 'divider_line' ) ) { ?>
                                <hr class="hairline"  data-aos="fade-up">
                            <?php } ?>
                        
                            <div class="grid-x grid-margin-x grid-padding-y" data-aos="fade-up">
                                <?php 
                                $h_col_num = count( get_sub_field( 'highlight_column' ) );
                                $h_col_class = 'medium-2';
                                if( $h_col_num == 3 ) {
                                    $h_col_class = 'medium-4';
                                } elseif( $h_col_num == 4 ) {
                                    $h_col_class = 'medium-3';
                                }
                                while( have_rows('highlight_column') ) : the_row(); ?>
                                    <div class="cell small-12 <?php echo $h_col_class; ?>">
                                        <ul class="no-bullets text-center">    
                                            <?php while( have_rows('highlights') ) : the_row(); ?>
                                                <li class="font-sans smaller-text"><?php the_sub_field('highlight'); ?></li>
                                            <?php endwhile; ?>
                                        </ul>
                                    </div>
                                <?php endwhile; ?>
                                <?php if( is_page( 'residences' ) ) { ?>
                                <div class="cell text-center section section--no-pad-bottom">
                                    <p class="smaller-text"><em>*in select units</em></p>
                                </div>
                                <?php } ?>
                            </div>
                        <?php endwhile; ?>
                    <?php endif; ?>
                    
                </div>
            </section>
        <?php elseif( get_row_layout() == 'amenities_tab_block' ) : ?>
        <section class="section section--amenties-tabs section--extra-pad light-bg">
            <div class="grid-container">
                <div class="grid-x grid-y-padding">
                    <div class="cell">
                        <ul class="tabs tabs--amenities no-bullets">
                            <?php $t=0; while( have_rows('amenities_tabs') ) : the_row(); $t++; 
                            $tab_slug = get_sub_field( 'amenity_tab_name'); 
                            $tab_slug = str_replace(array(' ', '-'), '', $tab_slug );
                            $tab_slug = strtolower($tab_slug);
                            ?>
                                <li class="<?php if( $t==1) echo 'current-tab'; ?>"><a href="#<?php echo $tab_slug; ?>"><?php the_sub_field( 'amenity_tab_name' ); ?></a></li>
                            <?php endwhile; ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="grid-container">
                <div class="tab-wrapper">
                    <?php $x=0; while( have_rows('amenities_tabs') ) : the_row(); $x++; ?>
                        <?php
                        $tab_slug = get_sub_field( 'amenity_tab_name');
                        $tab_slug = str_replace(array(' ', '-'), '', $tab_slug );
                        $tab_slug = strtolower($tab_slug); ?>
                        
                        <div id="<?php echo $tab_slug; ?>" class="tab-content">
                            <div class="grid-x grid-y-padding">
                                <div class="cell small-12 medium-6">
                                    <div class="amenity-map">
                                        <?php $a_hero = get_sub_field( 'amenity_hero' ); 
                                        echo wp_get_attachment_image($a_hero['ID'], 'large' ); ?>
                                        <div class="nodes nodes--<?php echo $tab_slug; ?>" data-slider="slider-<?php echo $tab_slug; ?>">
                                            <?php $n=0; while( have_rows( 'amenity_content') ) : the_row(); $n++; ?>
                                                <a id="<?php echo $tab_slug; ?>-node-<?php echo $n; ?>" href="#amenity-<?php echo $n; ?>" data-slide="<?php echo $n; ?>" class="node-marker <?php if( $n === 1 ) echo 'is-active'; ?>" data-node-name="<?php the_sub_field( 'amenity_headline' ); ?>">
                                                    <span class="node">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30">
                                                            <circle cx="15" cy="15" r="13" stroke-width="4">
                                                        </svg>
                                                    </span>
                                                </a>
                                            <?php endwhile; ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="cell small-12 medium-6 large-4 large-offset-1">
                                    
                                    <div id="slider-<?php echo $tab_slug; ?>" class="slider slider--node-slider slider--controls-bottom" data-index="<?php echo $tab_slug; ?>">
                                        <?php $a=0; while( have_rows( 'amenity_content') ) : the_row(); $a++; ?> 
                                            
                                            <div id="<?php echo $a; ?>" class="">
                                                <?php 
                                                $ac_image = get_sub_field( 'amenity_thumbnail' );
                                                if( $ac_image ) {
                                                    echo '<a href="'.$ac_image['sizes']['large'].'" class="js-image">';
                                                    echo wp_get_attachment_image($ac_image['ID'], 'medium' );
                                                    echo '</a>';
                                                } else {
                                                    echo '<img src="'.get_template_directory_uri().'/assets/images/placeholder.png" alt="Image Unavailable">';
                                                }
                                                
                                                ?>
                                                <h3><?php the_sub_field( 'amenity_headline' ); ?></h3>
                                                <?php the_sub_field('amenity_description'); ?>
                                            </div>
                                        <?php endwhile; ?>
                                    </div>
                                    <div class="slider-controls"></div>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                </div>
            </div>
        </section>
        <?php elseif( get_row_layout() == 'tabbed_slider' ) : ?>
            <section class="section section--tabbed_slider section--extra-pad light-bg">
            
                <div class="grid-container">
                    <div class="intro grid-x grid-y-padding align-center">
                        <div class="cell text-center" data-aos="fade-up">
                            <h2><?php the_sub_field( 'tab_header' ); ?></h2>
                        </div>
                    </div>
                </div>
                <div class="grid-container">
                    <div class="grid-x grid-y-padding">
                        <div class="cell">
                            <ul class="menu tab-slider-menu no-bullets">
                                <?php $i=0; while( have_rows( 'tabbed_sections' ) ) : the_row(); $i++; ?>
                                    <li <?php if($i===1) echo 'class="active"'; ?>><a href="#tab-<?php echo $i; ?>"><?php the_sub_field( 'tab_name' ); ?></a></li>
                                <?php endwhile; ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="grid-container">
                    <div class="grid-x grid-y-padding">
                        <div class="cell">
                            <div class="tab-slider">
                                <?php $j=0; while( have_rows( 'tabbed_sections' ) ) : the_row(); $j++; ?>
                                    <div id="tab-<?php echo $j; ?>" class="tab-container">
                                        <div class="grid-x grid-y-padding align-middle">
                                            <div class="cell medium-6">
                                                <?php 
                                                $img = get_sub_field( 'tab_image' ); 
                                                if($img && !empty($img)) { ?>
                                                    <img src="<?php echo $img['sizes']['large']; ?>" alt="<?php echo $img['alt']; ?>">
                                                <?php } else {
                                                    echo '<img src="https://placeholdit.co/i/800x545" alt="placeholder">';
                                                }?>
                                            </div>
                                            <div class="cell medium-4 medium-offset-1">
                                                <?php the_sub_field( 'tab_content' ); ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php endwhile; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        <?php elseif( get_row_layout() == 'call_to_action' ) : ?>
            <?php 
            $global_cta = get_sub_field( 'use_global_cta' ); 
            if( $global_cta ) {
                get_template_part( 'partials/content', 'cta' );
            } else { ?>
                <section class="section section--cta">
                    <div class="grid-container">
                        <div class="intro grid-x grid-y-padding align-center">
                            <div class="cell text-center" data-aos="fade-up">
                                <h2><?php the_sub_field( 'headline' ); ?></h2>
                            </div>
                        </div>
                        <div class="grid-x grid-y-padding">
                            <div class="cell medium-5 medium-offset-2" data-aos="fade-right">
                                <?php the_sub_field( 'description' ); ?>
                            </div>
                            <div class="cell medium-4  medium-offset-1" data-aos="fade-left">
                            <?php 
                                $button = get_sub_field( 'button' );
                                $btn_txt = $button['button_text'];
                                $btn_url = $button['button_url'];
                                if( !empty( $btn_txt ) && !empty( $btn_url ) ) {
                                    echo '<p><a href="'. esc_url( $btn_url ).'" class="button">'. esc_html( $btn_txt ) .'</a></p>';
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </section>
            <?php } ?>
        <?php elseif( get_row_layout() == 'neighborhood_map' ) : ?>
            <section class="section section--map neighborhood-map">
                <div class="grid-x map-section align-middle">
                    <div class="cell small-12 medium-9">
                        <div id="neighborhood-map" class="map map--locations"></div>
                    </div>
                    <div class="cell small-12 medium-3">
                        <div class="inner transparent-bg">
                            <h3 class="display-h3"><?php the_sub_field('map_title'); ?></h3>
                            <?php
                            $terms = get_terms(array(
                                'taxonomy' => 'wpseo_locations_category',
                                'exclude' => array( 3 ),
                                'hide_empty' => 1,
                                'orderby' => 'slug',
                            ));
                            ?>
                            <ul class="filter-items no-bullets map-filter">
                                <!-- <li><a href="javascript:;" class="cat-all is-active" data-category="all-categories">All</a></li> -->
                                <?php
                                //var_dump( $terms );
                                foreach ( $terms as $category ) {

                                    $locations = get_locations( $category );
                                    $cat_title = $category->name;
                                    $cat_nicename = $category->slug; ?>
                                    <li>
                                        <a href="javascript:;" class="cat-<?php echo $cat_nicename; ?>" data-category="<?php echo $cat_nicename; ?>">
                                            <svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/dist/sprite.svg#<?php echo $cat_nicename; ?>"></use></svg> <?php echo $cat_title; ?>
                                        </a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
            <section class="section section--favorites">
                <div class="grid-container fluid">
                    <div class="grid-x grid-padding-y">
                        <div class="cell">
                            <h3 class="display-h2 text-center m-b-3">Local Favorites</h3>
                        </div>
                    </div>
                    <div class="grid-x grid-margin-x align-center">
                        <?php
                        foreach( $terms as $category ) {
                            $locations = get_locations( $category );
                            $cat_title = $category->name;
                            $cat_nicename = $category->slug; 
                            ?>
                            
                            <div class="cell small-12 medium-2">
                                <div class="location-col">
                                    <h4 class="cat-<?php echo $cat_nicename; ?>"><?php echo $cat_title; ?></h4>
                                    <ul class="locations no-bullets m-b-3">
                                        <?php foreach( $locations as $key => $location ) : 
                                            if( $location->ID == 179 || $location->ID == 219 ) continue; ?>
                                            <li data-location-id="<?php echo $location->ID; ?>"><?php echo $location->post_title; ?></li>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                            </div>
                            
                        <?php } ?>
                    </div>
                </div>
            </section>
        <?php elseif( get_row_layout() == 'simple_map' ) : ?>
            <section class="section section--simple-map section--pad-bottom">
                <div class="grid-container  pattern pattern--left-side">
                    <div class="grid-x">
                        <div class="cell" data-aos="fade-up">
                            <div id="map"></div>
                        </div>
                    </div>
                </div>
            </section>
        <?php elseif( get_row_layout() == 'partner_grid' ) : ?>
            <section class="section section--partner-grid section--pad-bottom">
                <div class="grid-container pattern pattern--left-side">
                    <div class="grid-x grid-margin-x">
                        <div class="cell small-12 text-center m-b-3">
                            <h3><?php the_sub_field('headline'); ?></h3>
                        </div>
                    </div>
                    <div class="grid-x grid-margin-x grid-margin-y">
                        <?php $p=0; 
                        while( have_rows( 'partners' ) ) : the_row(); 
                            $p++; 
                            $a_delay = $p * 150;
                            $partner = get_sub_field('partner' );
                            $p_logo = get_sub_field( 'partner_logo' );
                            $offer = get_sub_field( 'offer' );
                            $o_desc = get_sub_field( 'offer_description' );
                            ?>
                            <div class="cell small-6 medium-3 large-3 partner" data-aos="fade-up" data-aos-delay="<?php echo $a_delay; ?>">
                                <a href="#partner-<?php echo $p; ?>" title="<?php echo $partner; ?>" class="js-inline">
                                    <?php echo wp_get_attachment_image( $p_logo['ID'], 'full' ); ?>
                                </a>
                                <div id="partner-<?php echo $p; ?>" class="mfp-hide partner-container">
                                    <div class="p-logo">
                                        <?php echo wp_get_attachment_image( $p_logo['ID'], 'full' ); ?>
                                    </div>
                                    <div class="p-offer">
                                        <h5><?php echo $partner; ?></h5>
                                        <h2><?php echo $offer; ?></h2>
                                        <p><?php echo $o_desc; ?></p>
                                    </div>
                                </div>
                            </div>
                        <?php 
                        endwhile; ?>
                    </div>
                </div>
            </section>
        <?php endif; ?>
    
    
    <?php endwhile; ?>
<?php endif; ?>