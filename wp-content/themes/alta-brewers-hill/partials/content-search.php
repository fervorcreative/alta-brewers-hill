<?php
/**
 * Template part for displaying results in search pages
 */
?>

<div class="cell large-auto">
	<article <?php post_class( 'entry' ); ?>>
		<header class="entry__header">
			<h2 class="entry__title"><?php the_title(); ?></h2>
		</header>

		<div class="entry__summary">
			<?php 
			if( has_excerpt() ) the_excerpt(); ?>
			<p><a href="<?php the_permalink(); ?>">View page</a></p>
		</div>
	</article>
</div>

