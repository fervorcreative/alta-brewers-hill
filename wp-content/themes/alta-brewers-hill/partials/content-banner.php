<?php

$banner_headline = get_field( 'main_heading' , 'option' );
$banner_subtext = get_field( 'supporting_text', 'option' );
$banner_link = get_field( 'banner_link', 'option' );

if( $banner_headline ) { ?>
<div class="banner">
    <h3 class="banner__text"><?php echo $banner_headline; ?></h3>
    <?php if( $banner_subtext ) echo '<p class="banner__text">'. $banner_subtext .'</p>'; ?>
    <button class="banner__close js-close-banner"></button>
</div>
<?php } ?>