<?php
/**
 * Template Name: Gallery Template
 */

get_header(); ?>

<main id="main" role="main">

<?php if ( have_posts() ) : ?>

    <?php while ( have_posts() ) : the_post(); ?>
    
        <?php get_template_part('partials/content', 'banner' ); ?>
        
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <div class="gallery-container section">
                
                <section id="gallery-categories" class="">
                    <div class="grid-container">
                        <div class="grid-x grid-margin-x grid-margin-y">
                            <?php $x=0; while( have_rows('galleries') ) : the_row(); $x++; 
                                $gal_title = get_sub_field( 'gallery_title' );
                                $gal_slug = strtolower($gal_title);
                                $cover = get_sub_field( 'cover_shot' );
                                $aos_delay = ( $x>1) ? $x * 50 : 0;
                            ?>
                            
                            <div class="cell small-12 medium-6 text-center" data-aos="fade-up" data-aos-delay="<?php echo $aos_delay; ?>">
                                <a href="#galleries" data-gallery="#gal-<?php echo $gal_slug; ?>" class="gs-link">
                                    <div class="gallery-cover" style="background-image: url( <?php echo $cover['sizes']['medium']; ?>;">
                                        <h3 class="display-h2"><?php echo $gal_title; ?></h3>
                                        <div class="overlay"></div>
                                    </div>
                                </a>
                            </div>
                            <?php endwhile; ?>
                        </div>
                    </div>
                </section>
                
                <section id="galleries" class="">
                    <div class="grid-x grid-padding-x align-middle">
                        <div class="cell medium-8">
                        <?php if( have_rows( 'galleries' ) ) : ?>
                            <div class="tab-wrapper gallery-wrapper">
                                <?php $i=0; while( have_rows('galleries') ) : the_row(); $i++; ?>
                                
                                    <?php 
                                    $gal_title = get_sub_field( 'gallery_title' );
                                    $gal_slug = strtolower($gal_title);
                                    ?>
                                    <div id="gal-<?php echo $gal_slug; ?>" class="tab-content <?php //if( $i==1) echo 'current-tab'; ?>">
                                    <?php while( have_rows('gallery') ) : the_row(); ?>
                                        <?php if( get_row_layout() == 'image_gallery' ) : ?>
                                            <?php $images = get_sub_field( 'images'); ?>
                                            <div class="slider slider--single">
                                                <?php foreach( $images as $img ) :  ?>
                                                    <div><?php echo wp_get_attachment_image( $img['ID'], 'large-slider' ); ?></div>
                                                <?php endforeach; ?>
                                            </div>
                                        <?php elseif( get_row_layout() == 'video_gallery' ) : ?>
                                            <div class="slider slider--single">
                                                <?php $a=0; while( have_rows('videos') ) : the_row(); $a++;?>
                                                    <div>
                                                        <div class="video-thumbnail">
                                                            <a href="<?php the_sub_field( 'video_url' ); ?>" class="js-video">
                                                                <?php 
                                                                $vid_thumb = get_sub_field( 'video_thumbnail' ); 
                                                                if( $vid_thumb ) {
                                                                    echo wp_get_attachment_image( $vid_thumb['ID'], 'large-slider' );
                                                                } else {
                                                                echo '<img src="https://via.placeholder.com/1170x840?text=Video+'.$a.'" alt="">';
                                                                }
                                                                ?>
                                                                <div class="video-overlay">
                                                                    <svg class="icon icon--play icon--large"  aria-hidden="true"><use xlink:href="<?php echo get_template_directory_uri(); ?>/dist/sprite.svg#play"></use></svg>
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </div>
                                                <?php endwhile; ?>
                                            </div>
                                        <?php endif; ?>
                                    <?php endwhile; ?>
                                    </div><!-- end .tab-content -->
                                    
                                <?php endwhile; ?>
                            </div><!-- end .tab-wrapper -->
                            <?php else : ?>
                            <p style="color: white;">Nothing to see here.</p>
                        <?php endif; ?>
                        </div>
                        <div class="cell medium-4">
                            <ul class="tabs gallery-options no-bullets">
                                <?php 
                                $j=0; while( have_rows('galleries') ) : the_row(); $j++;
                                    $gal_title = get_sub_field( 'gallery_title' );
                                    $gal_slug = strtolower($gal_title); ?>
                                    <li class="gal-<?php echo $gal_slug; ?> <?php if( $j==1 ) echo 'current-tab'; ?>"><a href="#gal-<?php echo $gal_slug; ?>" class="js-tab"><?php echo $gal_title; ?></a></li>
                                <?php endwhile;  ?>
                            </ul>
                        </div>
                    </div>
                    
                
                </section>
            </div>
            <?php get_template_part( 'partials/content', 'cta' ); ?>
        
        </article>

    <?php endwhile; ?>
    
<?php else : ?>
    <?php get_template_part( 'partials/content', 'none' ); ?>
<?php endif; ?>

</main>
<?php get_footer(); ?>