<!doctype html>
<html lang="en">
  <head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-PDCFBG9');</script>
    <!-- End Google Tag Manager -->
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no" />
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <?php wp_head(); ?>
	<script type="text/javascript"> 
      if (typeof RCTPCampaign != "undefined") {             
        RCTPCampaign.PropertyAPIKey = 'QzAwMDAwMDA2ODU3IzExNjEyOTMjYjZiMmQ4M2YtZDlkNi00Zjg5LTkzN2UtZGRiNDc0YTc0MGIx-wAlAuHt3nh0%3d';
        RCTPCampaign.init();         
      }     
      
      if (typeof ClickTrack != "undefined") {
        ClickTrack.SiteSection = 'PropertyPortal';
        ClickTrack._PageDisplayName = 'Lead Creation';
        ClickTrack.PropertyAPIKey = 'QzAwMDAwMDA2ODU3IzExNjEyOTMjYjZiMmQ4M2YtZDlkNi00Zjg5LTkzN2UtZGRiNDc0YTc0MGIx-wAlAuHt3nh0%3d'; 
        ClickTrack.init();
      }
     </script>
  </head>
  <body <?php if ( get_body_class() ) body_class(); ?>>
  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PDCFBG9"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->

    <?php
      // Dynamically add sprite.svg if it was created
      $sprite_path = get_template_directory_uri() . '/dist/sprite.svg';

      if ( file_exists( $sprite_path ) ) { ?>
        <script type="text/javascript">
          //Grab SVG Sprite and AJAX in so it can be cached
          var ajax = new XMLHttpRequest();
          ajax.open("GET", "<?php echo get_template_directory_uri(); ?>/dist/sprite.svg", true);
          ajax.onload = function(e) {
            var svg = $(ajax.responseText);
            document.body.insertBefore(svg.get(0), document.body.childNodes[0]);
          }
          ajax.send();
        </script>
      <?php }
    ?>
    <div id="nav-panel" class="nav-panel">
		<div class="nav-panel-top">
			<div class="grid-x small-up-2">
				<div class="cell">
					<div class="logo-icon text-left">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo get_bloginfo( 'name' ); ?>">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo-crown-white.svg" alt="<?php echo get_bloginfo( 'name' ); ?>">
						</a>
					</div>
				</div>
				<div class="cell text-right">
					
					<a href="#" class="js-nav-close" role="button">
						<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewport="0 0 36 36" width="36" height="36">
							<line x1="1" y2="1" stroke="white" stroke-width="1" y1="36" x2="36"></line>
							<line x1="1" y1="1" stroke="white" stroke-width="1" x2="36" y2="36"></line>
						</svg>
					</a>
				</div>
			</div>
		</div>
		<div class="nav-panel-middle">
			<?php wp_nav_menu( array(
				//'theme_location' => 'menu-header',
				'menu_class' => 'vertical menu no-bullets',
				'container' => 'nav',
				'container_class' => 'panel-nav primary'
			) ); ?>
			<a href="https://www.on-site.com/apply/property/308208" target="_blank" class="button">Apply Now</a>
			<nav class="panel-nav secondary">
				<?php
				$phone = get_field( 'phone_number', 'option' );
				if( $phone ) {
					$call_number = str_replace( array('-', '.', '(', ')'), '', $phone );
				} ?>
				<ul class="menu vertical small  no-bullets">
					<?php if( $phone ) { 
						echo '<li><a class="dniphonehref" href="tel:+1'. $call_number .'"><span class="dniphone">'. $phone .'</span></a>'; } ?></li>
					<li><a href="https://yccservices.yardi.com/i3root/webchat/webchat.php?chatID=243153" class="new-window">Live Chat</a></li>
					<li style="display: none;"><a href="">Text Us</a></li>
					<li><a href="/alta-advantage/">Alta Advantage</a></li>
					<li><a href="https://altabrewershill.securecafe.com/residentservices/alta-brewers-hill/userlogin.aspx" target="_blank">Resident Login</a></li>
				</ul>
			</nav>
			
		</div>
		<div class="nav-panel-bottom">
			<?php
			$social_links = array(); 
			$fb = get_field( 'facebook', 'option');
			if( $fb ) $social_links['Facebook'] = $fb;
			$tw = get_field( 'twitter', 'option');
			if( $tw ) $social_links['Twitter'] = $tw;
			$ig = get_field( 'instagram', 'option');
			if( $ig ) $social_links['Instagram'] = $ig;
			
			echo '<small>';
			foreach( $social_links as $link_name => $link ) {
				echo '<a href="'. esc_url( $link ) .'" target="_blank">'. $link_name .'</a>';
			}
			echo '</small>';
			?>
		</div>
      
    </div>
	<div class="site">
		<a href="#main" class="screen-reader-text">Skip to main content</a>
		<header class="header grid-x grid-padding-x grid-padding-y align-middle">
			<div class="cell small-12 medium-auto">
				<?php
				$phone = get_field( 'phone_number', 'option' );
				if( $phone ) {
					$call_number = str_replace( array('-', '.', '(', ')'), '', $phone ); ?>
					<span class="phone"><a class="dniphonehref" href="tel:+1<?php echo $call_number; ?>"><span class="dniphone"><?php echo $phone; ?></span></a></span>
				<?php } ?>
			</div>
			<div class="cell small-8 medium-auto">
				<div class="logo">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo get_bloginfo( 'name' ); ?>">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo-full-white.svg" alt="<?php echo get_bloginfo( 'name' ); ?>">
					</a>
				</div>
			</div>
			<div class="cell small-4 medium-auto text-right">
				<span class="apply-link">
					<a href="https://www.on-site.com/apply/property/308208" target="_blank">Apply Now</a>
				</span>
				
				<a href="#nav-panel" class="nav-trigger js-nav-open" role="button">
					<span></span>
				</a>
			</div>
		</header>
      