/* global AppData, google, ScrollMagic, TweenLite, Sine, AOS,successes */
// Set up App object and jQuery
var App = App || {},
  $ = $ || jQuery;

App.navLink = function() {
    $('.js-nav-open').on('click', function (e) {
        e.preventDefault();
        // Show nav overlay
        if ( ! $('html').hasClass('nav-open')) {
            $('.nav--header').fadeToggle(200);
            $('html, body').toggleClass('nav-open');
        // Hide nav overlay
        } /*else {
            $('.nav--header').fadeToggle(250);
            window.setTimeout(function() {
                $('html, body').toggleClass('nav-open');
            }, 50);
        }*/
    });
    $('.js-nav-close').on('click', function (e) {
        e.preventDefault();
        if ($('html').hasClass('nav-open')) {
            $('.nav--header').fadeToggle(250);
            window.setTimeout(function() {
                $('html, body').toggleClass('nav-open');
            }, 50);
        }
    });
};

App.heroAnimations = function() {
    var $image = $('.hero__logo-mark');

    function imageLoaded() {
        // Start animation by adding class once logo loads
        $('body').addClass('js-loaded');
    }
    
    $image.each(function() {
        if ( this.complete ) {
            // If image is cached
            imageLoaded();
        } else {
            // If image is loaded
            $(this).one('load', imageLoaded);
        }
    });
}

App.scrollTextAcross = function() {
  var controller = new ScrollMagic.Controller();

  $('.js-scroll-across').each(function() {
    var textWidth = $(this).outerWidth(),
      startPos = -(textWidth / 3),
      endPos = '30%';

    // If a short word that should start further in
    if ( $(this).hasClass('scroll-text--short') ) {
      startPos = '10%';
      endPos = '60%';
    }

    var tween = TweenLite.fromTo(this, 1, {right: startPos}, {right: endPos, ease: Sine.easeOut});

    new ScrollMagic.Scene({
      triggerElement: this,
      triggerHook: 'onEnter',
      duration: '110%',
      offset: 0
    })
    .setTween(tween)
    .addTo(controller);
  });
}

App.fadeAnimations = function() {
  AOS.init({
    disable: 'tablet',
    offset: 200,
    duration: 1000,
    once: true,
  });
}
App.imageGallery = function () {
    $('.js-image').magnificPopup({
        type: 'image'
    });
    $('.js-zoom').magnificPopup({
        type: 'image',
        mainClass: 'mfp-with-zoom',
        zoom: {
            enabled: true,
            duration: 300,
            easing: 'ease',
            opener: function (openerElement) {
                return openerElement.is('img') ? openerElement : openerElement.find('img');
            }
        },
        gallery: {
            enabled: true
        }
    });
}
App.videoPopup = function () {
    $('.js-link a').on('click', function (e) {
        e.preventDefault();
        var $target = $(this).attr('href');
        console.log($target);
        $.magnificPopup.open({
            items: {
                src: $target
            },
            type: 'iframe'
        });
    });
    $('.js-video').magnificPopup({
        type: 'iframe',
        mainClass: 'mfp-with-zoom',
        zoom: {
            enabled: true,
            duration: 300,
            easing: 'ease',
            opener: function (openerElement) {
                return openerElement.is('img') ? openerElement : openerElement.find('img');
            }
        },
        gallery: {
            enabled: true
        }
    });
}
App.inlinePopup = function () {
    /*
    $('.js-inline').on('click', function (e) { 
        e.preventDefault();
        var $target = $(this).attr('href');
        $.magnificPopup.open({
            items: {
                src: $target
            },
            type: 'inline'
        }, 0);
    });
    */
   $('.js-inline').magnificPopup({
        type: 'inline',
        mainClass: 'mfp-with-zoom',
        zoom: {
            enabled: true,
            duration: 300,
            easing: 'ease'
        },
        gallery: {
            enabled: true
        }
   });
    
    $('.iframe-popup a').on('click', function (e) {
        e.preventDefault();
        var $target = $(this).attr('href');
        $.magnificPopup.open({
            items: {
                src: $target
            },
            type: 'inline'
        }, 0);
    });
    
}
App.contactMap = function() {

    // Create custom Google Map
    var lat = 39.279187;
    var lng = -76.563688;
    var mapOptions = {
        zoom: 17,
        scrollwheel: false,
        zoomControl: true,
        saturation: -100,
        center: new google.maps.LatLng(lat, lng),
        disableDefaultUI: true,
        styles: [
          {
              'featureType': 'water',
              'elementType': 'geometry',
              'stylers': [
                  {
                      'color': '#e9e9e9'
                  },
                  {
                      'lightness': 17
                  }
              ]
          },
          {
              'featureType': 'landscape',
              'elementType': 'geometry',
              'stylers': [
                  {
                      'color': '#f5f5f5'
                  },
                  {
                      'lightness': 20
                  }
              ]
          },
          {
              'featureType': 'road.highway',
              'elementType': 'geometry.fill',
              'stylers': [
                  {
                      'color': '#ffffff'
                  },
                  {
                      'lightness': 17
                  }
              ]
          },
          {
              'featureType': 'road.highway',
              'elementType': 'geometry.stroke',
              'stylers': [
                  {
                      'color': '#ffffff'
                  },
                  {
                      'lightness': 29
                  },
                  {
                      'weight': 0.2
                  }
              ]
          },
          {
              'featureType': 'road.arterial',
              'elementType': 'geometry',
              'stylers': [
                  {
                      'color': '#ffffff'
                  },
                  {
                      'lightness': 18
                  }
              ]
          },
          {
              'featureType': 'road.local',
              'elementType': 'geometry',
              'stylers': [
                  {
                      'color': '#ffffff'
                  },
                  {
                      'lightness': 16
                  }
              ]
          },
          {
              'featureType': 'poi',
              'elementType': 'geometry',
              'stylers': [
                  {
                      'color': '#f5f5f5'
                  },
                  {
                      'lightness': 21
                  }
              ]
          },
          {
              'featureType': 'poi.park',
              'elementType': 'geometry',
              'stylers': [
                  {
                      'color': '#dedede'
                  },
                  {
                      'lightness': 21
                  }
              ]
          },
          {
              'elementType': 'labels.text.stroke',
              'stylers': [
                  {
                      'visibility': 'on'
                  },
                  {
                      'color': '#ffffff'
                  },
                  {
                      'lightness': 16
                  }
              ]
          },
          {
              'elementType': 'labels.text.fill',
              'stylers': [
                  {
                      'saturation': 36
                  },
                  {
                      'color': '#333333'
                  },
                  {
                      'lightness': 40
                  }
              ]
          },
          {
              'elementType': 'labels.icon',
              'stylers': [
                  {
                      'visibility': 'off'
                  }
              ]
          },
          {
              'featureType': 'transit',
              'elementType': 'geometry',
              'stylers': [
                  {
                      'color': '#f2f2f2'
                  },
                  {
                      'lightness': 19
                  }
              ]
          },
          {
              'featureType': 'administrative',
              'elementType': 'geometry.fill',
              'stylers': [
                  {
                      'color': '#fefefe'
                  },
                  {
                      'lightness': 20
                  }
              ]
          },
          {
              'featureType': 'administrative',
              'elementType': 'geometry.stroke',
              'stylers': [
                  {
                      'color': '#fefefe'
                  },
                  {
                      'lightness': 17
                  },
                  {
                      'weight': 1.2
                  }
              ]
          }
      ]
    }

    var map = new google.maps.Map(document.getElementById('map'), mapOptions);

    var markerImage = {
        url: AppData.template_dir + '/assets/images/map-icons/marker-home.svg',
        size: new google.maps.Size(58, 67),
    };
    var myLatLng = new google.maps.LatLng(lat, lng);
    var mapMarker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        icon: markerImage,
        url: 'https://goo.gl/maps/9uTAGECm1cwWhnHX9',
        anchor: new google.maps.Point(24, 24)
    });

    google.maps.event.addListener(mapMarker, 'click', function() {
      if (this.url) {
        window.open(this.url, '_blank');
      }
    });
}

// Location Map
App.locationsMap = function () {
    var lat = 39.279187;
    var lng = -76.563688;
    var zoomLevel = 15;
    
    var map_trigger = $('[data-location-id]'),
        filter_trigger = $('[data-category]'),
        map_options = {
            center: new google.maps.LatLng(lat, lng),
            mapTypeControl: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            navigationControl: false,
            navigationControlOptions: {
                style: google.maps.NavigationControlStyle.DEFAULT
            },
            scrollwheel: false,
            streetViewControl: false,
            zoom: zoomLevel,
            panControl: false,
            styles: [
                {
                    'featureType': 'water',
                    'elementType': 'geometry',
                    'stylers': [
                        {
                            'color': '#e9e9e9'
                        },
                        {
                            'lightness': 17
                        }
                    ]
                },
                {
                    'featureType': 'landscape',
                    'elementType': 'geometry',
                    'stylers': [
                        {
                            'color': '#f5f5f5'
                        },
                        {
                            'lightness': 20
                        }
                    ]
                },
                {
                    'featureType': 'road.highway',
                    'elementType': 'geometry.fill',
                    'stylers': [
                        {
                            'color': '#ffffff'
                        },
                        {
                            'lightness': 17
                        }
                    ]
                },
                {
                    'featureType': 'road.highway',
                    'elementType': 'geometry.stroke',
                    'stylers': [
                        {
                            'color': '#ffffff'
                        },
                        {
                            'lightness': 29
                        },
                        {
                            'weight': 0.2
                        }
                    ]
                },
                {
                    'featureType': 'road.arterial',
                    'elementType': 'geometry',
                    'stylers': [
                        {
                            'color': '#ffffff'
                        },
                        {
                            'lightness': 18
                        }
                    ]
                },
                {
                    'featureType': 'road.local',
                    'elementType': 'geometry',
                    'stylers': [
                        {
                            'color': '#ffffff'
                        },
                        {
                            'lightness': 16
                        }
                    ]
                },
                {
                    'featureType': 'poi',
                    'elementType': 'geometry',
                    'stylers': [
                        {
                            'color': '#f5f5f5'
                        },
                        {
                            'lightness': 21
                        }
                    ]
                },
                {
                    'featureType': 'poi.park',
                    'elementType': 'geometry',
                    'stylers': [
                        {
                            'color': '#dedede'
                        },
                        {
                            'lightness': 21
                        }
                    ]
                },
                {
                    'elementType': 'labels.text.stroke',
                    'stylers': [
                        {
                            'visibility': 'on'
                        },
                        {
                            'color': '#ffffff'
                        },
                        {
                            'lightness': 16
                        }
                    ]
                },
                {
                    'elementType': 'labels.text.fill',
                    'stylers': [
                        {
                            'saturation': 36
                        },
                        {
                            'color': '#333333'
                        },
                        {
                            'lightness': 40
                        }
                    ]
                },
                {
                    'elementType': 'labels.icon',
                    'stylers': [
                        {
                            'visibility': 'off'
                        }
                    ]
                },
                {
                    'featureType': 'transit',
                    'elementType': 'geometry',
                    'stylers': [
                        {
                            'color': '#f2f2f2'
                        },
                        {
                            'lightness': 19
                        }
                    ]
                },
                {
                    'featureType': 'administrative',
                    'elementType': 'geometry.fill',
                    'stylers': [
                        {
                            'color': '#fefefe'
                        },
                        {
                            'lightness': 20
                        }
                    ]
                },
                {
                    'featureType': 'administrative',
                    'elementType': 'geometry.stroke',
                    'stylers': [
                        {
                            'color': '#fefefe'
                        },
                        {
                            'lightness': 17
                        },
                        {
                            'weight': 1.2
                        }
                    ]
                }
            ]
        },
        map = new google.maps.Map(document.getElementById('neighborhood-map'), map_options),
        infowindow = new google.maps.InfoWindow(),
        markers = [],
        infowindows = [];
    
    map_trigger.on('click', function () {
        var location_id = parseInt($(this).attr('data-location-id'));
        
        infowindow.close();
        infowindow.setContent(infowindows[location_id]);
        infowindow.open(map, markers[location_id]);
        map.panTo(markers[location_id].getPosition());
        //markers[location_id].setZIndex(google.maps.Marker.MAX_ZINDEX + 1);
        $('html,body').stop().animate({
            'scrollTop': $('.neighborhood-map').offset().top - 40
        }, 1000, 'swing');

    });
    /*
    map_trigger.click( function(e) {
        e.preventDefault();

        var location_id = parseInt( $(this).attr('data-location-id') );

        infowindow.close();
        infowindow.setContent( infowindows[location_id] );
        infowindow.open( map, markers[location_id] );
        map.panTo(markers[location_id].getPosition());
        markers[location_id].setZIndex(google.maps.Marker.MAX_ZINDEX + 1);

        $('html,body').stop().animate({
            'scrollTop': $('#location-map').offset().top - 200
        }, 400, 'swing');
    });
    */
    // Trigger filter on click
    filter_trigger.on('click', function(e) {
        e.preventDefault();
        $('.filter-items li a').removeClass( 'is-active' );
        //var $locations = $('.map__locations > li'),
        var	category = $(this).attr('data-category');

        if( $(this).hasClass( 'is-active' ) ) {
            category = '';
        }


        filterLocations( category );

        // Change class on filters
        //$('.cat-btn').removeClass('is-active');
        $(this).addClass('is-active');

        // Change class on locations
        /*$locations.removeClass('is-active');
        $locations.parent().find('.map__locations-' + category).addClass('is-active');*/

        // Update dropdown
        //filter_dropdown.val( category );
        
        // scroll window to map
        // $('html,body').stop().animate({
        //     'scrollTop': $('#map').offset().top - 200
        // }, 400, 'swing' );
        
    });

    function filterLocations( category ) {
        console.log( category );
        var bounds = new google.maps.LatLngBounds(), i;

        // Defaults category to ???? for manual trigger
        if ( !category ) {
            category = 'all-categories';
        }
        // Go through and hide/show markers while calculating the bounds
        for ( i = 0; i < markers.length; i++ ) {

            if ( markers[i] ) {
                if(markers[i].category === 'home' || category === 'all-categories' ) {
                    markers[i].setMap(map);
                    bounds.extend(markers[i].getPosition());

                } else if ( markers[i].category !== category ) {
                    markers[i].setMap(null);
                    //console.log( 'flag this area');
                } else {
                    markers[i].setMap(map);
                    bounds.extend(markers[i].getPosition());
                }
            }
        }
        // Set map bounds
            map.setCenter(bounds.getCenter());
            map.fitBounds(bounds);
        // Remove one zoom level to ensure no marker is on the edge.
        map.setZoom(map.getZoom());

    };
    /*
    // set home marker above all markers
    new google.maps.Marker({
        position: new google.maps.LatLng(lat, lng),
        map: map,
        title: "Alta Union House",
        icon: AppData.template_dir + '/images/map-icons/marker-home.png',
        zIndex: google.maps.Marker.MAX_ZINDEX + 1
    });
    */
    //console.log( AppData.template_dir + '/images/map-icons/icon-art-and-culture.png' );
    for ( var i = 0; i < AppData.locations.length; i += 1 ) {
        (function () {
            var location = AppData.locations[i],
            latitude = location.custom_fields._wpseo_coordinates_lat,
            longitude = location.custom_fields._wpseo_coordinates_long,
            latLng = new google.maps.LatLng(latitude, longitude),
                icon = {
                //url: AppData.template_dir + '/assets/images/map-icons/marker-home.svg',
                url: AppData.template_dir + '/assets/images/map-icons/marker-' + location.category.slug + '.svg',
                //url: AppData.template_dir + '/images/map-icons/marker.png',
                //scaledSize: new google.maps.Size(50, 60),
                //origin: new google.maps.Point(0,0), // origin
                //anchor: new google.maps.Point(24, 48) // anchor
            },
            marker = new google.maps.Marker({
                map: map,
                title: location.post_title,
                position: latLng,
                zIndex: 1,
                category: location.category.slug
            }),
            content = [
                '<div class="infowindow">',
                '<strong>' + location.post_title + '</strong><br>',
                '<span>' + location.custom_fields._wpseo_business_address + '</span><br>',
                '<span>' + location.custom_fields._wpseo_business_city + ', ' + location.custom_fields._wpseo_business_state + ' ' + location.custom_fields._wpseo_business_zipcode + '</span><br>',
                //'<span>'+ location.custom_fields._wpseo_business_phone +'</span>',
                //'<span>' + '<a href="http://' + location.custom_fields._wpseo_business_url + '" target="_blank">View Website</a>' + '</span>',
                '</div>'
            ].join('');

            markers[location.ID] = marker;
            infowindows[location.ID] = content;

            marker.setIcon(icon);

            google.maps.event.addListener( marker, 'click', function() {
                infowindow.close();
                infowindow.setContent(content);
                infowindow.open(map, this);
                this.setZIndex(google.maps.Marker.MAX_ZINDEX + 1);
            });

            google.maps.event.addDomListener(window, 'resize', function() {
                var mapCenter = map.getCenter();
                google.maps.event.trigger(map, 'resize');
                map.setCenter(mapCenter);
            });

        }());
    }
}
App.galleryScroll = function () {
    $('.gs-link').on('click', function (e) {
        e.preventDefault();
        var galName = $(this).data('gallery');
        var gallerynoHash = galName.substring(1);
        //console.log(galName);
        // scroll down; activate tab
        // $('#galleries').fadeIn(600);
        // $('html, body').stop().animate({
        //     scrollTop: $('#galleries').offset().top - 115
        // }, 600, 'swing', function() {
        //     //window.location.hash = galName;
        // });
        $('#gallery-categories').addClass('gallery--open').delay(1000).addClass('gallery--hide');
        $('#galleries, .gallery-container').addClass('gallery--open');
        
        $('.tabs li').removeClass('current-tab');
        $('.tabs li.' + gallerynoHash ).addClass('current-tab');
        //$(galName).addClass('current-tab');
        $('.tab-content:visible').hide();
        $('html, body').stop().animate({
            scrollTop: $('#galleries').offset().top - 115
        }, 600, 'swing');
        setTimeout(function(){
            $(galName).fadeIn('slow', function () {
                $('.slider').slick('setPosition', 0);
            });
        },500);
        // $(galName).fadeIn('slow', function () {
        //     $('.slider').slick('setPosition', 0);
        // });
        //console.log('TAP tab-link');
    });
}
App.tabbedSection = function () {
    $('.tab-content:not(:first)').hide();
    $('.tabs li a').on('click', function (e) {
        e.preventDefault();
        
        $('.tabs li').removeClass('current-tab');
        $(this).parent().addClass('current-tab');
        
        var currentTab = $(this).attr('href');
        //$(currentTab).addClass('current-tab');
        $('.tab-content:visible').fadeOut(200, function () {
            $(currentTab).fadeIn(600, function () {
                $('.slider').slick('setPosition', 0);
            });
        });
        //console.log('vTAP tab-link');
    
    });
}
App.tabSliders = function () {
    $('.tab-slider').slick({
        slidesToShow: 1,
        infinite: false,
        dots: false,
        arrows: true,
        responsive: [
            {
                breakpoint: 960,
                settings: {
                    arrows: false
                }
            }
        ]
    });
    $('.tab-slider').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
        $('.tab-slider-menu li').removeClass('active');
        $('.tab-slider-menu li:eq(' + nextSlide + ')').addClass('active');
        
        console.log('Current Slide: ' + currentSlide + '. Next Slide: ' + nextSlide);
      });
    
    $('.tab-slider-menu li a').on('click', function (e) {
        e.preventDefault();
        
        var $this = $(this),
            slideIndex = $this.parent().index();
            //the_slider = $this.parent().data('slider');
        
        $this.parent().siblings().removeClass('active');
        $this.parent().addClass('active');
        $('.tab-slider').slick( 'slickGoTo', slideIndex );
    });
}
App.imageSliders = function () {
    $('.slider--carousel').slick({
        slidesToShow: 1, 
        centerMode: true,
        infinite: true,
        centerPadding: '200px',
        variableWidth: false,
        responsive: [{
            breakpoint: 768,
            settings: {
                centerPadding: '100px',
            },
            breakpoint: 480,
            settings: {
                centerPadding: '20px',
            }
        }]
    });
    $('.slider--single').slick({
        slidesToShow: 1, 
        infinite: true
    });
    
    
/* sliders controlled by nodes */
    var $nodeSlider = $('.slider--node-slider');
    
    $nodeSlider.each(function () {
        var $this = $(this),
            slider_index = $this.data('index'); // e.g., floor1, floor2
        
        
        
        $this.slick({
            fade: true,
            dots: false,
            slidesToShow: 1,
            adaptiveHeight: true,
            appendArrows: $this.next('.slider-controls'),
            //prevArrow: '<a href="#" class="slick-prev prev-arrow"><svg class="icon arrow"><use xlink:href="/wp-content/themes/alta-brewers-hill/dist/sprite.svg#arrow-left"></use></svg></a>',
            //nextArrow: '<a href="#" class="slick-next next-arrow"><svg class="icon arrow"><use xlink:href="/wp-content/themes/alta-brewers-hill/dist/sprite.svg#arrow-right"></use></svg></a>'
        });
        
        $this.on('afterChange', function (event, slick, currentSlide) {
            //updateNodes(slick, currentSlide);
            console.log('Slide changed. Current slide is now: ' + currentSlide);
            $('.nodes--' + slider_index + ' a').removeClass('is-active');
            $('.nodes--' + slider_index + ' a:eq(' + currentSlide + ')').addClass('is-active');
        });
        
    });
    
    $('.nodes a').on('click', function (e) {
        e.preventDefault();
        
        var $this = $(this),
            $node_parent = $this.parent(),
            slider = $this.parent().data('slider'),
            slide_index = $(this).data('slide') - 1;
        
        console.log('Slider: ' + slider + '. Slide Index: ' + slide_index);
        $node_parent.find('a').removeClass('is-active');
        $this.addClass('is-active');
        $('#' + slider).slick('slickGoTo', slide_index);
    });
    $('.node-marker').hover(function () {
        // on
        var $this = $(this),
            node = $this.find('.node'),
            node_name = $this.data('node-name');
        $this.css('z-index', 99999);
        $('<div class="tooltip"></div>')
            .text(node_name)
            .appendTo( node )
            .fadeIn('slow');
        //console.log(node_name);
    }, function () {
        // off
        $(this).css('z-index', 10);
        $('.tooltip').remove();
    });
}
App.banner = function() {
    var cond = localStorage.getItem('bannerClosed');

    localStorage.removeItem('bannerClosed');

    if (cond !== null) {
        $('.banner').hide();
    } else {
        $('.js-close-banner').on('click', function() {
            $('.banner').fadeOut(300);
            localStorage.setItem('bannerClosed', true);
        });
    }
    
    //var headerHeight = $('.header').height();
    if (cond === null) {
        var stickyPosition = $('.banner').offset().top;
        //var stickyTrigger
        var header_h = $('.header').height();
        var viewport = $(window).innerWidth();
        if (viewport <= 1022) {
            $('.banner').css('top', header_h);
        }
        $(window).on('scroll', function () {
            var scroll = getCurrentScroll();
            //console.log($('.banner').position());
            if (scroll > stickyPosition - header_h ) {
                $('.banner').addClass('banner--sticky');
                $('.banner').css('top', header_h);
            } /*else {
                $('.banner').removeClass('banner--sticky');
                $('.banner').css('top', 'auto');
            }*/
        });
    }
}
App.headerAdjustment = function () {
    var header_h = $('.header').height();
    var viewport = $(window).innerWidth();
    console.log('Viewport: ' + viewport + '. Header: ' + header_h);
    if (viewport >= 768) {
        $('#main').css('padding-top', header_h);
        $('.banner').css('top', header_h);
    }
    
}
App.PopupWindow = function() {
	$('.new-window').on('click', function(e) {
		e.preventDefault();
		var target = $(this).attr('href');
		var params = 'scrollbars=no,resizable=no,status=no,location=no,toolbar=no,menubar=no,width=530,height=570';
		window.open( target, 'Chat', params );
	});
}
function getCurrentScroll() {
    return window.pageYOffset || document.documentElement.scrollTop;
}


/* Tour Form
***********************************/
App.tourForm = function () {
    $('#form-messages').hide();
    
    $('#date-field').datepicker({
        class: 'date-field',
        minDate: new Date(),
        /*
        onRenderCell: function (date, cellType) {
        if (cellType == 'day') {
            var day = date.getDay(),
                isDisabled = disabledDays.indexOf(day) != -1;

            return {
            disabled: isDisabled
            }
        }
        },*/
        language: 'en',
        onSelect: function onSelect(date) {
            console.log('User has chosen: ' + date);
            loadTimeSlots(date);
        }
    });

    function loadTimeSlots(chosen_date) {
        var months = [
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December'
        ];
        if ( ! $('#timeslots').hasClass('hidden') ) {
            $('#timeslots').addClass('hidden');
        }
        var selected_date = new Date(chosen_date);
    
        var api_url = 'https://marketingapi.rentcafe.com/marketingapi/api/appointments/AvailableSlots',
            MarketingAPIKey = 'b6b2d83f-d9d6-4f89-937e-ddb474a740b1',
            //MarketingAPIKey = 'd1eaffc8-ec15-4be1-9e5e-d2b393f3b9eb', // new one ??? 
            CompanyCode = 'c00000006857',
            PropertyCode = 'p1136049',
            tMonth = selected_date.getMonth(),
            tDay = selected_date.getDate();
        
        
        $.ajax({
            url: api_url,
            type: 'POST',
            data: {
                MarketingAPIKey: MarketingAPIKey,
                CompanyCode: CompanyCode,
                PropertyCode: PropertyCode
            },
            dataType: 'JSON',
            beforeSend: function() {
                $('#timeslots').html('');
                if (! $('.loader').hasClass('.hidden') ) {
                    $('.loader').removeClass('hidden');
                }
                $('.loader').html('<p>We\'re looking up some time slots. Please wait.</p>');
            },
            success: function (data) {
                
                if (data.Response === null) { 
                    $('.loader').html('<p>No timeslots available</p>');
                    $('<p>' + data.ErrorMessage + '</p>').insertAfter('.loader');
                } else {
                    $('#timeslots').prepend('<option value="" selected>Choose a time slot</option>').removeClass('hidden');
                    $('.loader').addClass('hidden').html('');
                    
                    //console.log( data.Response.0.AvailableSlots );
                    //console.log( data );
                    //modelHTML += '<select id="timeslots" class="dropdown>';
                    var successes = [];
                    $.each(data.Response[0], function(index, value) {
                        //now you can access properties using dot notation
                        //console.log( 'index: ' + index + '. Value: ' + value );
                        
                        $.each(value, function (i, v) {
                            //if(i>10) return false;
                            //console.log( v.dtStart );
                            var startTime = new Date(v.dtStart),
                                //endTime = v.dtEnd,
                                stMonth = startTime.getMonth(),
                                stDay = startTime.getDate();
                                //stHour = startTime.getHours(),
                                //stMin = startTime.getMinutes();
                            
                            if (stMonth === tMonth) {
                                if (stDay === tDay) {
                                    successes.push({ starttime : v.dtStart, tourtype: v.TypeofSlot });
                                }
                            }
                            
                            /*
                            if( stHour > 12 ) {
                                stHour -= 12;
                            } else if( stHour === 0) {
                                stHour = 12;
                            }
                            */
                            //var displayHours = pad(stHour) + ':' + pad(stMin);
                            // if( stMonth === tMonth && stDay === tDay ) {
                            //     $('#timeslots').append('<option value="'+startTime.toLocaleTimeString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true})+'">'+startTime.toLocaleTimeString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true}) +' (' + tourType_text +')</option>');   
                            // }
                            //console.log( 'Start Month: '+ stMonth +'. Test Month: ' + tMonth );
                        });
                    });
                    // let the user know what's available (or not available)
                    if (successes.length) {
                        // console.log('Days that match: ' + successes.length);
                        //console.dir(successes);
                        for (var j = 0; j < successes.length; j++) {
                            //console.log('index: ' + j + '. Value: ' + successes[j]);
                            //console.log( successes[j].starttime );
                            var start_time = new Date( successes[j].starttime ),
                                tourType = successes[j].tourtype,
                                tourType_text = '';
                            if (tourType === 'GuidedTour') {
                                tourType_text = 'Guided Tour';
                            } else if(tourType === 'Personal') {
                                tourType_text = 'Personal Tour';
                            } else if(tourType === 'VirtualTour') {
                                tourType_text = 'Virtual Tour';
                            }
                            $('#timeslots').append('<option value="' + start_time.toLocaleTimeString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true }) + '">' + start_time.toLocaleTimeString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true }) +' ('+ tourType_text +')</option>'); 
                            /*
                            if (typeof successes[j] !== 'undefined') {

                            //console.log( 'index: ' + j + '. Value: ' + successes[j] );
                            var start_time = new Date(successes[j]);
                            $('#timeslots').append('<option value="' + start_time.toLocaleTimeString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true }) + '">' + start_time.toLocaleTimeString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true }) +'</option>'); 
                            }
                            */
                        }

                    } else {
                        $('#timeslots').html('').addClass('hidden');
                        $('.loader').removeClass('hidden').html('<p>There are no time slots available for '+ months[tMonth] +' '+ tDay +'</p>');
                    }
                    //$('#test').html(modelHTML);
                }
                
                
                
                
            },
            error: function (errorMessage) { // error callback 
                $('.loader').html('<p>Error: ' + errorMessage + '</p>');
            }
        });
    }
    $('.gf_tour_form').validate({
        rules: {
            ApptDate: 'required',
            ApptTime: 'required',
            FirstName: 'required',
            LastName: 'required',
            Email: {
                required: true,
                email: true
            },
            Phone: {
                required: true,
                phoneUS: true
            }
        },
        errorClass: 'gfield_description',
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.insertAfter( element.parent() );
        },
        submitHandler: function (form) {
            //var addlData = {'MarketingAPIKey':'b6b2d83f-d9d6-4f89-937e-ddb474a740b1','CompanyCode':'C00000006857','PropertyCode': 'p1189627'}
            var serializeData = $(form).serialize(),
                propertyCode = 'p1136049';
        
            $.ajax({
                url: 'https://marketingapi.rentcafe.com/marketingapi/api/appointments/createleadwithappointment?MarketingAPIKey=b6b2d83f-d9d6-4f89-937e-ddb474a740b1&CompanyCode=C00000006857&PropertyCode='+ propertyCode,
                type: 'POST',
                data: serializeData,
                success: function (data) {
                    console.log('data: ' + data);
                    // $('#form-messages').html('<p>Your tour request has been submitted. You will be hearing from us soon!</p>').show();
                    // $('.gf_tour_form').hide();
                    window.location.href = '/thank-you/';
                },
                error: function (errorMessage) { // error callback 
                    //$('#form-messages').html('<p>Error: ' + errorMessage + '</p>');
                    console.log('Error: ' + errorMessage);
                }
            });
        }
    });
}


// Instantiate functions when document is ready
$(function() {
    App.headerAdjustment();
    App.heroAnimations();
    App.banner();
    App.navLink();
    App.imageGallery();
    App.videoPopup();
    App.tabbedSection();
    App.imageSliders();
    App.tabSliders();
    App.PopupWindow();
    App.inlinePopup();
    if ( $('body').hasClass('page-template-page-gallery') ) {
        App.galleryScroll();   
    }
    if ($('body').hasClass('page-template-page-tour')) {
        App.tourForm();
    }
});

// Instantiate functions when document has loaded most assets such as images
$(window).on('load', function() {
    // App.scrollTextAcross();
    App.fadeAnimations();
    if ( $('body').hasClass('page-neighborhood') ) {
        App.locationsMap();
    } else {
        App.contactMap();
    }
    
    
    
   
});

