<?php
/**
 * Page template file
 */

get_header(); ?>

<main id="main" role="main">

<?php if ( have_posts() ) : ?>

    <?php while ( have_posts() ) : the_post(); ?>
    
    <?php get_template_part('partials/content', 'banner' ); ?>
    
    <?php get_template_part( 'partials/content', 'hero' ); ?>
    
    <?php get_template_part( 'partials/content', 'builder' ); ?>
    
    <?php endwhile; ?>
  
<?php else : ?>
    <?php get_template_part( 'partials/content', 'none' ); ?>
<?php endif; ?>

<?php get_template_part( 'partials/content', 'helix' ); ?>

</main>

<?php get_footer(); ?>
