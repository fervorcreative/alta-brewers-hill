<?php
/**
 * Template Name: Schedule a Tour
 */

get_header(); ?>

<main id="main" role="main">

<?php if ( have_posts() ) : ?>

  <?php while ( have_posts() ) : the_post(); ?>

    <?php get_template_part('partials/content', 'banner' ); ?>
    
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
      <?php get_template_part( 'partials/content', 'hero' ); ?>
      
      <section class="section">
        <div class="grid-container">
          <div class="grid-x grid-margin-x text-center">
            <div class="cell small-12 large-10 large-offset-1">
              
              <h3 class="display-h2"><?php the_field( 'headline' ); ?></h3>
              <?php the_field( 'intro_copy' ); ?>
              
              <div class="tour-form" data-aos="fade-up">
                <div class="gform_wrapper">
                  <div id="form-messages" class="text-center"></div>
                  <form action="" class="gf_tour_form" method="post" enctype="multipart/form-data">
                    <div class="gform_body">
                      <ul class="gform_fields">
                        <li class="gfield gfield_span">
                          <label class="gfield_label" for="ApptDate">Tour Date</label>
                          <div class="ginput_container">
                            <input id="date-field" name="ApptDate" type="text" class="input date-field" value="" required placeholder="Tour Date">
                          </div>
                        </li>
                        <li class="gfield gfield_span">
                          <label class="gfield_label" for="ApptTime">Tour Time</label>
                          <div class="ginput_container">
                            <select id="timeslots" name="ApptTime" class="dropdown hidden" required></select>
                            <div class="loader"><p>Choose a date</p></div>
                          </div>
                        </li>
                        <li class="gfield">
                          <label class="gfield_label" for="FirstName">First Name</label>
                          <div class="ginput_container">
                            <input type="text" name="FirstName" class="text" value="" placeholder="First Name" required>
                          </div>
                        </li>
                        <li class="gfield">
                          <label class="gfield_label" for="LastName">Last Name</label>
                          <div class="ginput_container">
                            <input type="text" name="LastName" class="text" value="" placeholder="Last Name" required>
                          </div>
                        </li>
                        <li class="gfield">
                          <label class="gfield_label" for="Email">Email Address</label>
                          <div class="ginput_container">
                            <input type="email" name="Email" class="text" value="" placeholder="Email Address" required>
                          </div>
                        </li>
                        <li class="gfield">
                          <label class="gfield_label" for="Phone">Phone Number</label>
                          <div class="ginput_container">
                            <input type="text" name="Phone" class="text" value="" placeholder="Phone Number" required>
                          </div>
                        </li>
                        <li id="field_3_15" class="gfield gform_validation_container gfield_visibility_visible">
                          <label class="gfield_label" for="input_3_15">Phone</label>
                          <div class="ginput_container">
                            <input name="input_15" id="input_3_15" type="text" value="" autocomplete="off">
                          </div>
                          <div class="gfield_description" id="gfield_description__15">
                            This field is for validation purposes and should be left unchanged.
                          </div>
                        </li>
                      </ul>
                    </div>
                    <div class="gform_footer">
                      <?php
                      $floorplan = '';
                      if( isset( $_GET['floorplan']) ) {
                      $floorplan = $_GET['floorplan'];
                      } else {
                        $floorplan = 'No floor plan chosen.';
                      }
                      ?>
                      <input type="hidden" name="FloorPlan" class="text" value="<?php echo $floorplan; ?>">
                      <button id="tour-button" type="submit" value="Submit" name="Submit" class="gform_button button">
                      Submit
                      </button>
                      <!-- <input type="submit" value="Submit" name="Submit" class="gform_button button"> -->
                    </div>
                  </form>
                </div>
              </div>
              
            </div>
          </div>
        </div>
      </section>
      
      <section class="section">
        <div class="grid-container">
          <div class="grid-x grid-margin-x">
            <div class="cell small-12 large-10 large-offset-1">
              <div id="map" class="map"></div>
            </div>
          </div>
        </div>
      </section>
    </article>
  
  <?php endwhile; ?>
<?php endif; ?>
<?php get_template_part( 'partials/content', 'helix' ); ?>

</main>
<?php get_footer(); ?>