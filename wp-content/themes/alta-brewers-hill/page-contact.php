<?php
/**
 * Template Name: Contact Template
 */

get_header(); ?>

<main id="main" role="main">

<?php if ( have_posts() ) : ?>

    <?php while ( have_posts() ) : the_post(); ?>
    
    <?php get_template_part('partials/content', 'banner' ); ?>
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <?php get_template_part( 'partials/content', 'hero' ); ?>
        <section class="section">
            <div class="grid-container">
                <div class="grid-x">
                    <div class="cell small-12 large-3 large-offset-1">
                        <h3 class="display-h2"><?php the_field( 'headline' ); ?></h3>
                        <?php the_field( 'main_content' ); ?>
                    </div>
                    <div class="cell small-12 large-3 large-offset-1 contact-details text-smaller">
                        <h4 class="display-h3 text-uppercase">Contact Info</h4>
                        <p class="font-sans text-smaller">
                            <?php the_field( 'address', 'option' ); ?><br>
                            <?php the_field( 'city', 'option' ); ?>, <?php the_field( 'state', 'option' ); ?> <?php the_field( 'zip', 'option' ); ?>
                        </p>
                        <?php
                        $phone = get_field( 'phone_number', 'option' );
                        $call_number = str_replace( array('-', '(', ')'), '.', $phone );
                        $email = get_field( 'email', 'option' );
                        ?>
                        <p class="font-sans text-smaller">
                            <a href="/schedule-a-tour/">Schedule a Tour</a><br>
                            <?php if( !empty( $phone ) ) { ?>
                                <a class="dniphonehref" href="tel:+1<?php echo $call_number; ?>"><span class="dniphone"><?php echo $phone; ?></span></a><br>
                            <?php } ?>
                            <?php if( !empty( $email ) ) { ?>
                                <a href="mailto:<?php echo antispambot( $email ); ?>">Email Us</a>
                            <?php } ?>
                        </p>
                        <?php 
                        $office_hours = get_field( 'office_hours', 'option' ); 
                        if( $office_hours ) { ?>
                            <div style="margin-top: 2.5rem;">
                                <h4 class="display-h3 text-uppercase">Office Hours</h4>
                                <p class="font-sans text-smaller"><?php echo $office_hours; ?></p>
                            </div>
                            
                        <?php } ?>
                    </div>
                    <div class="cell small-12 large-3">
                        <?php 
                        $form_id = ( get_field( 'form_id' ) ) ? get_field( 'form_id' ) : 1;
                        gravity_form( $form_id, false, false, false, null, true, 1, true ); ?>
                    </div>
                </div>
            </div>
        </section>
        <section class="section">
            <div class="grid-container">
                <div class="grid-x">
                    <div class="cell">
                        <div id="map" class="map"></div>
                    </div>
                </div>
            </div>
        </section>
    </article>
    
    
    
    
    <?php endwhile; ?>
  
<?php else : ?>
    <?php get_template_part( 'partials/content', 'none' ); ?>
<?php endif; ?>

</main>

<?php get_footer(); ?>
