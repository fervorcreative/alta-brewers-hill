<?php
/* Add ACF Options Pages
 * ------------------------------------------------------------------------------
 */

if ( function_exists( 'acf_add_options_page' ) ) {
	acf_add_options_page();
	acf_add_options_sub_page( 'Options' );
}


add_filter('acf/settings/save_json', 'acf_json_save_point');
function acf_json_save_point( $path ) {
    
    // update path
    $path = get_stylesheet_directory() . '/acf-json';
    
    // return
    return $path;
    
}