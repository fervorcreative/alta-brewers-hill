<?php
/**
 * Create [phone] shortcode that dynamically converts phone number to tel link
 */

add_shortcode( 'phone', 'shortcode_tel_link' );
function shortcode_tel_link( $atts, $string = null ) {
	// Use global phone number if empty
	if ( !$string ) {
		$string = get_field( 'phone', 'options' );
	}

	// Remove all characters and leave only numbers
	$tel = preg_replace('/[^0-9]+/', '', $string);

	if ( $atts ) {
		$class = ' class="' . $atts['class'] . '"';
	} else {
		$class = '';
	}

	// Check if number already has 1 in front
	if ( strlen( $tel ) === 11 && substr( $tel, 0, 1) === '1' ) {
		return '<a href="tel:+' . $tel . '"' . $class . '>' . $string . '</a>';
	} else {
		return '<a href="tel:+1' . $tel . '"' . $class . '>' . $string . '</a>';
	}
}

/**
 * Create [email] shortcode that obfuscates email from spam bots
 */

add_shortcode( 'email', 'shortcode_obfuscate_email' );
function shortcode_obfuscate_email( $atts, $content = null ) {
	if ( ! is_email( $content ) ) {
		return;
	}

	return '<a href="mailto:' . antispambot( $content ) . '">' . antispambot( $content ) . '</a>';
}


// custom admin login logo
add_action('login_head', 'abh_custom_login_logo');
function abh_custom_login_logo() {
	echo '<style type="text/css">
    body { background-color: #161616 !important; }
	h1 a { 
        background-image: url('.get_template_directory_uri().'/assets/images/logo-crown-large.svg) !important; 
        width: 150px !important; 
        height: 114px !important; 
		background-size: cover !important;
		margin: 0 auto 3em !important;
    }
    #wp-submit {
        border-radius: 0px !important;
        box-shadow: none !important;
        background: #ab8561 !important;
        border: none !important;
        text-shadow: none !important;
        text-transform: uppercase;
	}
	.login #backtoblog a, .login #nav a {
		color: #717171 !important;
	}
	
	.login #backtoblog a:hover, .login #nav a:hover {
		color: #ab8561 !important;
	}
	input[type="text"],
	input[type="email"],
	input[type="password"] {
		border-color: #b5b5b5 !important;
		box-shadow: none !important;
		border-radius: 0 !important;
	}
	input:focus {
		border-color: #ab8561 !important;
	}
	</style>';
}

function abh_add_slug_body_class( $classes ) {
	global $post;
	if ( isset( $post ) ) {
		$classes[] = $post->post_type . '-' . $post->post_name;
	}
	return $classes;
}
add_filter( 'body_class', 'abh_add_slug_body_class' );