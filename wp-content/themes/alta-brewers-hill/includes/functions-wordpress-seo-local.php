<?php
/**
 * Get locations from WordPress SEO Local
 */

function get_locations( $category = null ) {
	$query_args = array(
		'post_type' => 'wpseo_locations',
		'orderby' => 'title',
		'order' => 'ASC',
		'posts_per_page' => -1,
		'fields' => 'wpseo_coordinates_lat',
	);

	if ( $category ) {
		$query_args['tax_query'] = array (
			array (
					'taxonomy' => 'wpseo_locations_category',
					'field' => 'slug',
					'terms' => array('home', $category->slug ),
				)
		);
	}

	$query = new WP_Query( $query_args );

	// Append custom fields and categories
	foreach ( $query->posts as $i => $post ) {
		// Get custom fields values
		$custom_fields = get_post_custom( $post->ID );
		foreach ( $custom_fields as $j => $field ) {
			if ( 1 == count( $field ) ) {
				$custom_fields[$j] = reset( $field );
			}
		}

		// Append custom fields
		$query->posts[$i]->custom_fields = $custom_fields;

		// Get category and append
		$category = get_the_terms( $post->ID, 'wpseo_locations_category' );
		if ( 1 == count( $category ) ) {
			$category = reset( $category );
		}
		$query->posts[$i]->category = $category;
	}

	return $query->posts;
}
