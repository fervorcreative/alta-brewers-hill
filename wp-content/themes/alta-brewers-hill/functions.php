<?php
/**
 * Add Support for Custom Menus
 */

add_theme_support( 'menus' );
add_theme_support( 'title-tag' );
add_theme_support( 'post-thumbnails', array( 'post', 'page' ) );
add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption' ) );
add_post_type_support( 'page', 'excerpt' );

/**
 * Register Custom Menu Locations
 */

register_nav_menu( 'menu-header', 'Header' );
register_nav_menu( 'menu-footer', 'Footer' );

/**
 * Custom Thumbnail Sizes
 *
 * @link http://codex.wordpress.org/Function_Reference/add_image_size
 */

// add_image_size( 'hero', 1600, 500, true );
add_image_size( 'carousel', 953, 600, true );
add_image_size( 'large-slider', 1170, 840, true );
/**
 * Enqueue Theme CSS Files
 */

 function init_enqueue_css() {
  wp_enqueue_style( 'typekit-fonts', '//use.typekit.net/dhf0cfg.css', array(), null);
  wp_enqueue_style( 'theme', get_template_directory_uri() . '/dist/app.css', array(), null);
}

add_action( 'wp_enqueue_scripts', 'init_enqueue_css' );

/**
 * Enqueue Theme JavaScript Files
 */

add_action( 'wp_enqueue_scripts', 'init_enqueue_js' );

function init_enqueue_js() {
  if ( ! is_admin() ) {
    wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', '//cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js', false, false, false );
    wp_enqueue_script( 'jquery' );
  }
  $api_key = 'AIzaSyBnym6xCbvrO7RIo5WMWrrZ3RpTqCLgZx0';
  wp_enqueue_script( 'google-maps-api', 'https://maps.googleapis.com/maps/api/js?key=' . $api_key, array(), null, true );
  wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/assets/js/vendor/modernizr-custom.js', array(), '3.6.0', false );
  wp_enqueue_script('click-track', 'https://t.rentcafe.com/ytpclicktrack.min.js', array('jquery'), null, true);
  wp_enqueue_script('rent-cafe', 'https://cdngeneral.rentcafe.com/JS/ThirdPartySupport/LeadAttributionAndDNIv1.2.min.js', array('jquery'), null, true);
  //wp_enqueue_script( 'slick-js', 'https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.2/min/tiny-slider.js', array('jquery'), null, true );
  wp_enqueue_script( 'app', get_template_directory_uri() . '/dist/app.js', array( 'jquery' ), null, true );

  // Localize template URL for usage in JS
  $data = array(
    'template_dir' => get_stylesheet_directory_uri(),
    'locations' => get_locations()
  );

  wp_localize_script( 'app', 'AppData', $data );
}
//add_action( 'wp_footer', 'init_tiny_slider' );
function init_tiny_slider() { ?>
  <script type="module">
    var forEach = function( array, callback, scope ) {
      for( var i=0; i<array.length; i++ ) {
        callback.call(scope, i, array[i]);
      }
    };
    var sliders = document.querySelectorAll('.slider');
    forEach(sliders, function(index, value) {
      // fetch some parameters
      let items_to_display = value.dataset.items;
      let edge_padding = value.dataset.edgepadding;
      let gutter = value.dataset.gutter;
      let slider = tns({
        container: value,
        items: items_to_display,
        center: true,
        //center: true,
        edgePadding: edge_padding,
        gutter: gutter, // space between slides
        controls: true,
        controlsText: ['Prev', 'Next'],
        nav: false,
        mouseDrag: true
      });
      
      
      
    });
    
    /*
    var slider = tns({
      container: '.slider.slider--three',
      items: 2,
      //edgePadding: 50,
      //gutter: 16, // space between slides
      center: true,
      controls: true,
      controlsText: ['Prev', 'Next'], // markup
      nav: false,
      mouseDrag: true
    });
    
    var slider2 = tns({
      container: '.slider.slider--single',
      items: 1,
      //edgePadding: 50,
      //gutter: 16, // space between slides
      center: true,
      controls: true,
      controlsText: ['Prev', 'Next'], // markup
      nav: false,
      mouseDrag: true
    });
    */
  </script>
  <?php
}

/**
 * Add Google Analytics Tracking Code
 */

function init_ga_analytics() {
  $propertyID = 'UA-129351399-7'; // GA Property ID ?>

  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo $propertyID; ?>"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', '<?php echo $propertyID; ?>');
  </script>
<?php }

add_action( 'wp_head', 'init_ga_analytics' );

/**
 * Add theme utility functions
 */

require_once( get_template_directory() . '/includes/functions-defaults.php' );
require_once( get_template_directory() . '/includes/functions-helpers.php' );
require_once( get_template_directory() . '/includes/functions-acf.php' );
require_once( get_template_directory() . '/includes/functions-wordpress-seo.php' );
require_once( get_template_directory() . '/includes/functions-wordpress-seo-local.php' );


