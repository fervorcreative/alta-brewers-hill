<?php
/**
 * The front page template
 */

get_header(); ?>

<main id="main" role="main">

  <?php if ( have_posts() ) : ?>

    <?php while ( have_posts() ) : the_post(); ?>

      <div class="banner">
        <h3 class="banner__text">Coming Soon – Spring 2020</h3>
        <button class="banner__close js-close-banner"></button>
      </div>

      <section class="hero">
        <img
          src="<?php bloginfo('template_directory'); ?>/assets/images/logo-crown-large.svg"
          class="hero__logo-mark"
          alt="Alta Brewers Hill mark"
          aria-hidden
        >

        <div class="hero__logo">
          <img src="<?php bloginfo('template_directory'); ?>/assets/images/logo-alta-brewers-hill.svg" alt="Alta Brewers Hill">
        </div>
      </section>

      <section class="section section--no-pad-bottom section--keep-top">
        <div class="scroll-text-container">
          <h2 class="scroll-text scroll-text--main display-h1 color-primary js-scroll-across">Spirited Simplicity</h2>
        </div>

        <div class="grid-container">
          <div class="intro grid-x grid-padding-y align-center">
            <div class="cell text-center" data-aos="fade-up">
              <h1 class="display-h2">Dashing New Luxe Apartment Homes in Canton, MD</h1>
            </div>
          </div>

          <div class="pattern grid-x grid-padding-y">
            <div class="cell" data-aos="fade-up">
              <img src="<?php bloginfo('template_directory'); ?>/assets/images/friends-together.jpg" alt="Friends hanging out">
            </div>
          </div>
        </div>
      </section>

      <section class="section section--no-pad">
        <div class="grid-container">
          <h2 class="display-h1 scroll-text js-scroll-across">Residences</h2>

          <div class="grid-x grid-padding-y align-middle">
            <div class="cell medium-5 large-4 medium-offset-1 medium-order-2" data-aos="fade-left">
              <h3> Inspired Living</h3>
              <p>With reverence for its storied past and an eye to the future, Alta Brewers Hill embraces the neighborhood’s industrial vibe and elevates it, for a modern sophisticated design that’s both intriguing and inviting. But Alta is more than its aesthetics. It offers residents an experience that is at once familiar and surprising, and one that builds connections that grow into a community.</p>
            </div>
            <div class="cell medium-6" data-aos="fade-right">
              <img src="<?php bloginfo('template_directory'); ?>/assets/images/dog-play.jpg" alt="Woman playing with a puppy">
            </div>
          </div>

          <div class="pattern pattern--alt pattern--right-side grid-x grid-padding-y">
            <div class="cell" data-aos="fade-up">
              <img src="<?php bloginfo('template_directory'); ?>/assets/images/swimming-pool.jpg" alt="Friends at the pool">
            </div>
          </div>
        </div>
      </section>

      <section class="section section--no-pad">
        <div class="grid-container">
          <h2 class="display-h1 scroll-text scroll-text--short js-scroll-across">Amenities</h2>

          <div class="grid-x grid-padding-y align-middle">
            <div class="cell medium-5 large-4 medium-offset-1" data-aos="fade-right">
              <h3>Expertly Crafted</h3>
              <p>Enjoy your downtime, work when duty calls, and never stop learning. Relax by the courtyard pool or in the sports lounge. Wrap up a challenging project in the co-working space. Learn a new cooking technique in the Exhibition Kitchen. Every day at Alta Brewers Hill brings opportunities that cultivate wellness and new discoveries.</p>
            </div>
            <div class="cell medium-6 medium-offset-1" data-aos="fade-left">
              <img src="<?php bloginfo('template_directory'); ?>/assets/images/corn-hole.jpg" alt="Friends playing corn hole">
            </div>
          </div>

          <div class="pattern pattern--alt grid-x grid-padding-y">
            <div class="cell" data-aos="fade-up">
              <img src="<?php bloginfo('template_directory'); ?>/assets/images/restaurant.jpg" alt="Busy restaurant">
            </div>
          </div>
        </div>
      </section>

      <section class="section section--no-pad">
        <div class="grid-container">
          <h2 class="display-h1 scroll-text js-scroll-across">Neighborhood</h2>

          <div class="grid-x grid-padding-y align-middle">
            <div class="cell medium-5 large-4 medium-offset-1 medium-order-2" data-aos="fade-left">
              <h3>A Rich History</h3>
              <p>Brewers Hill takes its name from the landmark breweries it was once home to, and their presence is still felt in the architecture of this vibrant neighborhood. With an abundance of restaurants, pubs, shopping and dog parks, as well as unbeatable walkability and access to downtown and the suburbs via I-95, it’s easy to see why young professionals are making it their home.</p>
            </div>
            <div class="cell medium-6" data-aos="fade-right">
              <img src="<?php bloginfo('template_directory'); ?>/assets/images/working-out.jpg" alt="Working out together">
            </div>
          </div>
        </div>
      </section>

      <section class="section">
        <div class="grid-container pattern pattern--right-side">
          <div class="intro grid-x grid-padding-y align-center">
            <div class="cell text-center" data-aos="fade-up">
              <h2>Get in Touch to Join the Waitlist</h2>
            </div>
          </div>
          <div class="grid-x grid-padding-y">
            <div class="cell medium-4 medium-offset-2" data-aos="fade-right">
              <p>Discover spirited simplicity that makes life easier—and more enjoyable and exhilarating. Complete the form below to get the latest details and join the waitlist for Baltimore’s most popular new address.</p>
            </div>
            <div class="cell medium-3 medium-offset-1">
              <div class="form">
                <?php echo do_shortcode('[gravityform id="1" title="false" description="false" ajax="true"]'); ?>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section class="section section--pad-bottom">
        <div class="grid-container">
          <div class="grid-x">
            <div class="cell" data-aos="fade-up">
              <div id="map"></div>
            </div>
          </div>
        </div>
      </section>

    <?php endwhile; ?>

  <?php else : ?>

    <?php get_template_part( 'partials/content', 'none' ); ?>

  <?php endif; ?>

</main>

<?php get_footer(); ?>
