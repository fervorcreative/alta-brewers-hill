  <footer class="footer" role="contentinfo">
    <div class="grid-container">
      <div class="grid-x align-middle">
        <div class="cell medium-shrink">
          <img src="<?php bloginfo('template_directory'); ?>/assets/images/logo-crown.svg" alt="Alta Brewers Hill mark" class="footer__logo">
        </div>
        <div class="footer__copyright cell medium-auto">
          <p>
            1211 S Eaton St, Baltimore, MD 21224 <br>
            &copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?>
            <span> | </span>
            Professionally Managed by Wood Partners
            <span> | </span>
            <a href="https://www.woodpartners.com/privacy-policy/" target="_blank">Privacy Policy</a>
            <span> | </span>
            <a href="https://www.woodpartners.com/digital-accessibility/?_ga=2.172898019.1930056624.1579276808-1612636055.1579276808" target="_blank">Digital Accessibility</a>
          </p>
        </div>
        <div class="footer__icons cell large-shrink">
          <a href="https://www.woodpartners.com/" target="_blank">
            <img class="footer__icon-p" src="<?php bloginfo('template_directory'); ?>/assets/images/wood-p@2x.png" alt="Wood Partners">
          </a>
          <a href="https://www.woodpartners.com/property-management" target="_blank">
            <img class="footer__icon-r" src="<?php bloginfo('template_directory'); ?>/assets/images/wood-r@2x.png" alt="Wood Partners Residential Services">
          </a>
          <a href="https://www.woodpartners.com/wp-content/uploads/2017/11/WRS-Pet-Policy.pdf" target="_blank">
            <img class="footer__icon-pets" src="<?php bloginfo('template_directory'); ?>/assets/images/wood-pets-logo@2x.png" alt="WRS Loves Pets">
          </a>
          <a href="https://www.hud.gov/program_offices/fair_housing_equal_opp" target="_blank">
            <img class="footer__icon-f" src="<?php bloginfo('template_directory'); ?>/assets/images/f-icons@2x.png" alt="Equal Housing and Accessibility">
          </a>
        </div>
      </div>
    </div>
  </footer>
</div>

<?php wp_footer(); ?>

</body>
</html>
