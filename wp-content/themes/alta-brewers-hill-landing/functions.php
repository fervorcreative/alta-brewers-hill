<?php
/**
 * Add Support for Custom Menus
 */

add_theme_support( 'menus' );
add_theme_support( 'title-tag' );
add_theme_support( 'post-thumbnails', array( 'post', 'page' ) );
add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption' ) );
add_post_type_support( 'page', 'excerpt' );

/**
 * Register Custom Menu Locations
 */

register_nav_menu( 'menu-header', 'Header' );
register_nav_menu( 'menu-footer', 'Footer' );

/**
 * Custom Thumbnail Sizes
 *
 * @link http://codex.wordpress.org/Function_Reference/add_image_size
 */

// add_image_size( 'hero', 1600, 500, true );

/**
 * Enqueue Theme CSS Files
 */

 function init_enqueue_css() {
  wp_enqueue_style( 'typekit-fonts', '//use.typekit.net/dhf0cfg.css', array(), null);
  wp_enqueue_style( 'theme', get_template_directory_uri() . '/dist/app.css', array(), null);
}

add_action( 'wp_enqueue_scripts', 'init_enqueue_css' );

/**
 * Enqueue Theme JavaScript Files
 */

add_action( 'wp_enqueue_scripts', 'init_enqueue_js' );

function init_enqueue_js() {
  if ( ! is_admin() ) {
    wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', '//cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js', false, false, false );
    wp_enqueue_script( 'jquery' );
  }

  wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/assets/js/vendor/modernizr-custom.js', array(), '3.6.0', false );
  wp_enqueue_script( 'app', get_template_directory_uri() . '/dist/app.js', array( 'jquery' ), null, true );

  // Localize template URL for usage in JS
  $data = array(
    'template_dir' => get_stylesheet_directory_uri(),
  );

  wp_localize_script( 'app', 'AppData', $data );
}

/**
 * Add Google Analytics Tracking Code
 */

function init_ga_analytics() {
  $propertyID = 'UA-129351399-7'; // GA Property ID ?>

  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo $propertyID; ?>"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', '<?php echo $propertyID; ?>');
  </script>
<?php }

add_action( 'wp_head', 'init_ga_analytics' );

/**
 * Add theme utility functions
 */

require_once( get_template_directory() . '/includes/functions-defaults.php' );
// require_once( get_template_directory() . '/includes/functions-helpers.php' );
// require_once( get_template_directory() . '/includes/functions-acf.php' );
require_once( get_template_directory() . '/includes/functions-wordpress-seo.php' );
// require_once( get_template_directory() . '/includes/functions-wordpress-seo-local.php' );
