/* global AppData, google, ScrollMagic, TweenLite, Sine, AOS */

// Set up App object and jQuery
var App = App || {},
  $ = $ || jQuery;

App.navLink = function() {
  $('.js-nav-toggle').on( 'click', function() {
    // Show nav overlay
    if ($('html').hasClass('is-open')) {
      $('.nav--header').fadeToggle(200);
      $('html').toggleClass('is-open');
    // Hide nav overlay
    } else {
      $('.nav--header').fadeToggle(250);
      window.setTimeout(function() {
        $('html').toggleClass('is-open');
      }, 50);
    }
  });
};

App.heroAnimations = function() {
  var $image = $('.hero__logo-mark');

  function imageLoaded() {
    // Start animation by adding class once logo loads
    $('body').addClass('js-loaded');
  }

  $image.each(function() {
    if ( this.complete ) {
      // If image is cached
      imageLoaded();
    } else {
      // If image is loaded
      $(this).one('load', imageLoaded);
    }
  });
}

App.scrollTextAcross = function() {
  var controller = new ScrollMagic.Controller();

  $('.js-scroll-across').each(function() {
    var textWidth = $(this).outerWidth(),
      startPos = -(textWidth / 3),
      endPos = '30%';

    // If a short word that should start further in
    if ( $(this).hasClass('scroll-text--short') ) {
      startPos = '10%';
      endPos = '60%';
    }

    var tween = TweenLite.fromTo(this, 1, {right: startPos}, {right: endPos, ease: Sine.easeOut});

    new ScrollMagic.Scene({
      triggerElement: this,
      triggerHook: 'onEnter',
      duration: '110%',
      offset: 0
    })
    .setTween(tween)
    .addTo(controller);
  });
}

App.fadeAnimations = function() {
  AOS.init({
    disable: 'tablet',
    offset: 200,
    duration: 1000,
    once: true,
  });
}

App.contactMap = function() {
  $.getScript('https://www.google.com/jsapi', function() {
    google.load('maps', '3', {
      other_params: 'key=AIzaSyBnym6xCbvrO7RIo5WMWrrZ3RpTqCLgZx0',
      callback: function() {
        // Create custom Google Map
        var lat = 39.2789817;
        var lng = -76.5644441;
        var mapOptions = {
            zoom: 15,
            scrollwheel: false,
            saturation: -100,
            center: new google.maps.LatLng(lat, lng),
            disableDefaultUI: true,
            styles: [
              {
                  'featureType': 'water',
                  'elementType': 'geometry',
                  'stylers': [
                      {
                          'color': '#e9e9e9'
                      },
                      {
                          'lightness': 17
                      }
                  ]
              },
              {
                  'featureType': 'landscape',
                  'elementType': 'geometry',
                  'stylers': [
                      {
                          'color': '#f5f5f5'
                      },
                      {
                          'lightness': 20
                      }
                  ]
              },
              {
                  'featureType': 'road.highway',
                  'elementType': 'geometry.fill',
                  'stylers': [
                      {
                          'color': '#ffffff'
                      },
                      {
                          'lightness': 17
                      }
                  ]
              },
              {
                  'featureType': 'road.highway',
                  'elementType': 'geometry.stroke',
                  'stylers': [
                      {
                          'color': '#ffffff'
                      },
                      {
                          'lightness': 29
                      },
                      {
                          'weight': 0.2
                      }
                  ]
              },
              {
                  'featureType': 'road.arterial',
                  'elementType': 'geometry',
                  'stylers': [
                      {
                          'color': '#ffffff'
                      },
                      {
                          'lightness': 18
                      }
                  ]
              },
              {
                  'featureType': 'road.local',
                  'elementType': 'geometry',
                  'stylers': [
                      {
                          'color': '#ffffff'
                      },
                      {
                          'lightness': 16
                      }
                  ]
              },
              {
                  'featureType': 'poi',
                  'elementType': 'geometry',
                  'stylers': [
                      {
                          'color': '#f5f5f5'
                      },
                      {
                          'lightness': 21
                      }
                  ]
              },
              {
                  'featureType': 'poi.park',
                  'elementType': 'geometry',
                  'stylers': [
                      {
                          'color': '#dedede'
                      },
                      {
                          'lightness': 21
                      }
                  ]
              },
              {
                  'elementType': 'labels.text.stroke',
                  'stylers': [
                      {
                          'visibility': 'on'
                      },
                      {
                          'color': '#ffffff'
                      },
                      {
                          'lightness': 16
                      }
                  ]
              },
              {
                  'elementType': 'labels.text.fill',
                  'stylers': [
                      {
                          'saturation': 36
                      },
                      {
                          'color': '#333333'
                      },
                      {
                          'lightness': 40
                      }
                  ]
              },
              {
                  'elementType': 'labels.icon',
                  'stylers': [
                      {
                          'visibility': 'off'
                      }
                  ]
              },
              {
                  'featureType': 'transit',
                  'elementType': 'geometry',
                  'stylers': [
                      {
                          'color': '#f2f2f2'
                      },
                      {
                          'lightness': 19
                      }
                  ]
              },
              {
                  'featureType': 'administrative',
                  'elementType': 'geometry.fill',
                  'stylers': [
                      {
                          'color': '#fefefe'
                      },
                      {
                          'lightness': 20
                      }
                  ]
              },
              {
                  'featureType': 'administrative',
                  'elementType': 'geometry.stroke',
                  'stylers': [
                      {
                          'color': '#fefefe'
                      },
                      {
                          'lightness': 17
                      },
                      {
                          'weight': 1.2
                      }
                  ]
              }
          ]
        }

        var map = new google.maps.Map(document.getElementById('map'), mapOptions);

        var markerImage = {
            url: AppData.template_dir + '/assets/images/map-icons/marker-home.svg',
            size: new google.maps.Size(48, 48),
        };
        var myLatLng = new google.maps.LatLng(lat, lng);
        var mapMarker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            icon: markerImage,
            url: 'https://goo.gl/maps/9uTAGECm1cwWhnHX9',
            anchor: new google.maps.Point(24, 24)
        });

        google.maps.event.addListener(mapMarker, 'click', function() {
          if (this.url) {
            window.open(this.url, '_blank');
          }
        });
      }
    });
  });
}

App.banner = function() {
  var cond = localStorage.getItem('bannerClosed');

  // localStorage.removeItem('bannerClosed');

  if (cond !== null) {
    $('.banner').hide();
  } else {
    $('.js-close-banner').on('click', function() {
      $('.banner').fadeOut(300);
      localStorage.setItem('bannerClosed', true);
    });
  }
}

// Instantiate functions when document is ready
$(document).ready(function() {
  App.heroAnimations();
  App.banner();
  // App.navLink();
});

// Instantiate functions when document has loaded most assets such as images
$(window).on('load', function() {
  // App.scrollTextAcross();
  App.fadeAnimations();
  App.contactMap();
});
