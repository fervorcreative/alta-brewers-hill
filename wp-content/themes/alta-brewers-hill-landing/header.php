<!doctype html>
<html lang="en">
  <head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-PDCFBG9');</script>
    <!-- End Google Tag Manager -->
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no" />
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <?php wp_head(); ?>
  </head>
  <body <?php if ( get_body_class() ) body_class(); ?>>
  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PDCFBG9"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->

    <?php
      // Dynamically add sprite.svg if it was created
      $sprite_path = get_template_directory() . '/dist/sprite.svg';

      if ( file_exists( $sprite_path ) ) { ?>
        <script type="text/javascript">
          //Grab SVG Sprite and AJAX in so it can be cached
          var ajax = new XMLHttpRequest();
          ajax.open("GET", "<?php bloginfo('template_directory'); ?>/dist/sprite.svg", true);
          ajax.onload = function(e) {
            var svg = $(ajax.responseText);
            document.body.insertBefore(svg.get(0), document.body.childNodes[0]);
          }
          ajax.send();
        </script>
      <?php }
    ?>

    <div class="site">
      <a href="#main" class="screen-reader-text">Skip to main content</a>
