<?php
/**
 * The template for displaying Search Results pages
 */

get_header(); ?>

<main id="main" role="main">

  <?php if ( have_posts() ) : ?>

    <section class="section search-results">
      <div class="grid-container">

        <header class="grid-x">
          <div class="cell">
            <h1><?php printf( __( 'Search Results for: %s' ), '<span>' . get_search_query() . '</span>' ); ?>
          </div>
        </header>

        <div class="grid-x small-up-2">

          <?php while ( have_posts() ) : the_post(); ?>

            <?php get_template_part( 'partials/content', 'search' ); ?>

          <?php endwhile; ?>

          <?php the_posts_navigation(); ?>

        </div>
      </div>
    </section>

  <?php else : ?>

    <?php get_template_part( 'partials/content', 'none' ); ?>

  <?php endif; ?>

</main>

<?php get_footer(); ?>
