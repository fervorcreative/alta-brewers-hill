<?php
/**
 * Page template file
 */

get_header(); ?>

<main id="main" role="main">

  <?php if ( have_posts() ) : ?>

    <section class="section">
      <div class="grid-container">
        <div class="grid-x">
          <div class="cell">

            <?php while ( have_posts() ) : the_post(); ?>

              <?php get_template_part( 'partials/content', 'page' ); ?>

            <?php endwhile; ?>

          </div>
        </div>
      </div>
    </section>

  <?php else : ?>

    <?php get_template_part( 'partials/content', 'none' ); ?>

  <?php endif; ?>

</main>

<?php get_footer(); ?>
