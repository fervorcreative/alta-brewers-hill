<?php namespace RazzInteractiveFloorPlanBrowser;

/**
 * @wordpress-plugin
 * Plugin Name:       Zeki Floor Plan Browser
 * Description:       Add script to access the Razz Interactive Floor Plan Browser.
 * Version:           1.3.0
 * Author:            Razz Interactive
 * Author URI:        http://razzinteractive.com
 */

/** Start: Detect ACF Pro plugin. Include if not present. */
if (!class_exists('acf')) {
    // Define path and URL to the ACF plugin.
    define('MY_ACF_PATH', plugin_dir_path(__FILE__) . 'includes/acf/');

    // Include the ACF plugin.
    include_once(MY_ACF_PATH . 'acf.php');

    // (Optional) Hide the ACF admin menu item.
    add_filter('acf/settings/show_admin', 'my_acf_settings_show_admin');
    function my_acf_settings_show_admin($show_admin) {
        return false;
    }
}

require_once(dirname(__FILE__) . '/acf-assets/options-page.php');
require_once(dirname(__FILE__) . '/EnqueueScripts.php');

// Setup Options Page
new OptionsPage();

// Enqueue Styles/Scripts
new EnqueueScripts();
