<?php namespace RazzInteractiveFloorPlanBrowser;

class EnqueueScripts
{
    public function __construct()
    {
        add_action('wp_enqueue_scripts', [$this, 'enqueue_scripts']);
    }

    public function enqueue_scripts()
    {
        if (!function_exists('get_field')) return;

        $zeki_property_url = get_field('razz-zeki-property_url', 'option');

        if (!$zeki_property_url) return;

        // Strip '/' at the end if included
        if (substr($zeki_property_url, -1) === '/') {
            $zeki_property_url = substr($zeki_property_url, 0, -1);
        }

        $script = "{$zeki_property_url}/widgets/v1/overlay.js";

        $zeki_path = get_field('razz-zeki-path', 'option');
        if ($zeki_path) {
            $script .= "?path={$zeki_path}";
        }

        wp_enqueue_script('razz-zeki-remote-ifpb', $script, ['jquery'], '1.0.1', true);
    }
}
