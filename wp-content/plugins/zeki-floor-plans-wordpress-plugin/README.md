# Zeki Floor Plan Browser

A WordPress plugin for [Razz Interactive](https://www.razzinteractive.com) clients to connect their site to the Zeki Floor Plan Browser.

##Installation
1. Visit the settings page **(Settings -> Zeki Floor Plan Browser)** and enter the Property ID and URL provided to you by [Razz Interactive](https://www.razzinteractive.com).
2. Link to `#/floor-plans` from anywhere on your site you wish, and it will open the floor plan browser when clicked.
