<?php namespace RazzInteractiveFloorPlanBrowser;

class OptionsPage
{
    public function __construct()
    {
        add_action('acf/init', [$this, 'acf_init']);
    }

    function acf_init()
    {
        $this->setupOptions();

        $this->setupFields();
    }

    protected function setupOptions()
    {
        acf_add_options_sub_page(array(
            'page_title' => 'Zeki Floor Plan Browser',
            'menu_title' => 'Zeki Floor Plan Browser',
            'parent' => 'options-general.php',
            'capability' => 'manage_options',
        ));
    }

    protected function setupFields()
    {
        acf_add_local_field_group(array(
            'key' => 'group_580fac118e858',
            'title' => 'Zeki Floor Plan Browser Settings',
            'fields' => array(
                array(
                    'key' => 'field_5c7e9fa384dea',
                    'label' => 'Zeki Property URL',
                    'name' => 'razz-zeki-property_url',
                    'type' => 'text',
                    'instructions' => 'Please enter the Property URL provided by Razz Interactive.',
                    'required' => 1,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '50',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                ),
                array(
                    'key' => 'field_5c7e9fa384ddd',
                    'label' => 'Floor Plans Path',
                    'name' => 'razz-zeki-path',
                    'type' => 'text',
                    'instructions' => 'Optional. Use if you would like to open Zeki with a path other than `floor-plans`',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '50',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                ),
            ),
            'location' => array(
                array(
                    array(
                        'param' => 'options_page',
                        'operator' => '==',
                        'value' => 'acf-options-zeki-floor-plan-browser',
                    ),
                ),
            ),
            'menu_order' => 0,
            'position' => 'normal',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => 1,
            'description' => '',
        ));
    }
}
