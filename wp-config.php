<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'f/ODjLKu0cLaG8+jGO8ZYCldTFoay330+CFgVZbyKQETzx9xStFeNw2/sImlFocanCuNyVN3L+c9GfQvf6XeTw==');
define('SECURE_AUTH_KEY',  '9DH8/zQITR99Hvmgey++t/EhXvY8uKfiuKBemOe8ITTNrQIdeUGuj22dvKykiw+Mu9Xw+0sR4uszsUfK32GPSw==');
define('LOGGED_IN_KEY',    'f/Y8eGamsTs+oas37fgnZStnTPIGRPdxpgsPQv02uVMQ9+0waFQyxbKdNk3rf5FerqAl3JhLSWrqjdCS7pGktg==');
define('NONCE_KEY',        'XEiTvQlqXfmI3kuN6+xakvzL8Xuw8PUvq8Gag8SPWRqNzvi7xR4d+dAV6ASa7WBtp8kjP0PwlHuSsxe8varw9g==');
define('AUTH_SALT',        'lsSEyMFGSbj3d5+XIplEbZ6ELGnxLgAnJZkBLwFw5n+Qwup2ESBMRPNnHwBnJTVA01zePMjIm20/HKEgbl/WOQ==');
define('SECURE_AUTH_SALT', 'YBssdqjCBpV6E6BGAt5bxU9KZEJojQmYMrauh7/hbqo2GoiBLLN/PFyZW19GE0kk3jm2shYl6XJ+g4SFKqb7iA==');
define('LOGGED_IN_SALT',   'jKazK9BJ7IF198m5moZGc8hDh1WwrbaOQ8/riYayWbNOiRF9MCCmwLaYltxxhSgLI//oUi5WSZAtEhMQFQ0pbw==');
define('NONCE_SALT',       'pj+hEBnwq+Or88lYEN6volkdW/cRbduQKOmFMmHirknVcDBQsTBdYV4BC2Z3rzJhY6Apo6UCXf8eI8lyGI590w==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_rr6efri04p_';




/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
